#include "EcologyCohort.h"

EcologyCohort::EcologyCohort( ) {
    // Declare and attach eating formulations
    mEatingFormulations["Basic eating"] = new EatingSet( );
    // Declare and attach metabolism formulations
    mMetabolismFormulations["Basic metabolism"] = new MetabolismSet( );
    // Declare and attach mortality formulations
    mReproductionFormulations["Basic reproduction"] = new ReproductionSet( );
    // Declare and attach mortality formulations
    mMortalityFormulations["Basic mortality"] = new MortalitySet( );
}

void EcologyCohort::InitialiseEating( GridCell& gcl) {
    // Initialise eating formulations - has to be redone every step?
    mEatingFormulations["Basic eating"]->InitializeEcologicalProcess( gcl, "revised predation" );
    mEatingFormulations["Basic eating"]->InitializeEcologicalProcess( gcl, "revised herbivory" );
}

EcologyCohort::~EcologyCohort( ) {
    delete mEatingFormulations["Basic eating"];
    delete mMetabolismFormulations["Basic metabolism"];
    delete mMortalityFormulations["Basic mortality"];
    delete mReproductionFormulations["Basic reproduction"];
}

void EcologyCohort::RunWithinCellEcology( GridCell& gcl, Cohort* actingCohort, unsigned currentTimestep, ThreadVariables& partial, unsigned currentMonth ) {
    // RUN EATING
    if( actingCohort->mIndividualBodyMass > 0 ) {
        mEatingFormulations["Basic eating"]->RunEcologicalProcess( gcl, actingCohort, currentTimestep, partial, currentMonth );
        // RUN METABOLISM - THIS TIME TAKE THE METABOLIC LOSS TAKING INTO ACCOUNT WHAT HAS BEEN INGESTED THROUGH EATING
        mMetabolismFormulations["Basic metabolism"]->RunEcologicalProcess( gcl, actingCohort, currentTimestep, partial, currentMonth );
        // RUN REPRODUCTION - TAKING INTO ACCOUNT NET BIOMASS CHANGES RESULTING FROM EATING AND METABOLISING
        mReproductionFormulations["Basic reproduction"]->RunEcologicalProcess( gcl, actingCohort, currentTimestep, partial, currentMonth );
        // RUN MORTALITY - TAKING INTO ACCOUNT NET BIOMASS CHANGES RESULTING FROM EATING, METABOLISM AND REPRODUCTION
        mMortalityFormulations["Basic mortality"]->RunEcologicalProcess( gcl, actingCohort, currentTimestep, partial, currentMonth );
    }
}

void EcologyCohort::UpdateEcology( GridCell& gcl, Cohort* actingCohort, unsigned currentTimestep ) {
    // Apply the results of within-cell ecological processes
    mApplyEcologicalProcessResults.UpdateAllEcology( gcl, actingCohort, currentTimestep );
}

