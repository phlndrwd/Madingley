#ifndef INITIALISATION
#define INITIALISATION

#include "Types.h"
#include "FunctionalGroupDefinitions.h"
#include "UtilityFunctions.h"
#include "Grid.h"
#include "Stopwatch.h"
#include "Constants.h"
#include "Parameters.h"
#include "NonStaticSimpleRNG.h"

/** \ brief* Initialization information for Madingley model simulations */
class Initialisation {
public:
    Initialisation( );

    /** \brief Reads the initialization file to get information for the set of simulations to be run */
    Initialisation( Grid* );

private:
    
    /** \brief  Seed grid cells with cohorts, as specified in the model input files
    @param g A reference to a grid cell */
    long SeedGridCellCohorts( GridCell& );
    /** \brief    Seed grid cell with stocks, as specified in the model input files
    @param gcl The grid cell  */
    long SeedGridCellStocks( GridCell& );

    Stopwatch mInitializationTimer;
    NonStaticSimpleRNG mRandomNumber;
};

#endif
