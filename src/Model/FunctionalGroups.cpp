#include "FunctionalGroups.h"

Types::FunctionalGroupsPointer FunctionalGroups::mThis = NULL;

Types::FunctionalGroupsPointer FunctionalGroups::Get( ) {
    if( mThis == NULL ) mThis = new FunctionalGroups( );
    
    return mThis;
}

FunctionalGroups::FunctionalGroups( ) {

}

FunctionalGroups::~FunctionalGroups( ) {
    if( mThis != NULL ) delete mThis;
}

FunctionalGroupDefinitions& FunctionalGroups::GetStocks( ) {
    return mStocks;
}

FunctionalGroupDefinitions& FunctionalGroups::GetCohorts( ) {
    return mCohorts;
}

bool FunctionalGroups::SetStocks( std::string filePath ) {
    return mStocks.Initialise( filePath );
}

bool FunctionalGroups::SetCohorts( std::string filePath ) {
    return mCohorts.Initialise( filePath );
}