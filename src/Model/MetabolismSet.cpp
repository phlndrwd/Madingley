#include "MetabolismSet.h"
#include "FunctionalGroups.h"

MetabolismSet::MetabolismSet( ) {
    // Add the basic endotherm metabolism implementation to the list of implementations
    Implementations[ "basic endotherm" ] = new MetabolismEndotherm( );

    // Add the basic ectotherm metabolism implementation to the list of implementations
    Implementations[ "basic ectotherm" ] = new MetabolismEctotherm( );
}

MetabolismSet::~MetabolismSet( ) {
    delete Implementations[ "basic endotherm" ];
    delete Implementations[ "basic ectotherm" ];
}

void MetabolismSet::InitializeEcologicalProcess( GridCell& gcl, std::string implementationKey ) {

}

void MetabolismSet::RunEcologicalProcess( GridCell& gcl, Cohort* actingCohort, unsigned currentTimestep, ThreadVariables& partial, unsigned currentMonth ) {
    if( FunctionalGroups::Get( )->GetCohorts( ).GetTraitNames( "Heterotroph/Autotroph", actingCohort->mFunctionalGroupIndex ) == "heterotroph" ) {
        if( FunctionalGroups::Get( )->GetCohorts( ).GetTraitNames( "Endo/Ectotherm", actingCohort->mFunctionalGroupIndex ) == "endotherm" ) {
            Implementations[ "basic endotherm" ]->Run( actingCohort, currentTimestep, currentMonth );
        } else {
            Implementations[ "basic ectotherm" ]->Run( actingCohort, currentTimestep, currentMonth );
        }
    }
}
