#ifndef TIMESTEP
#define TIMESTEP

#include "Types.h"
#include "Constants.h"

class Timing {
public:
    ~Timing( );
    static Types::TimeStepPointer Get( );

    void Update( const unsigned& );
    
    unsigned GetTimeStep( const std::string& timeUnit = Constants::cMonthTimeUnitName );
    unsigned GetFirstYear( ) const;
    unsigned GetLastYear( ) const;
    
    std::string GetCurrentDateString( );

private:
    Timing( );

    static Types::TimeStepPointer mThis;

    unsigned mCurrentMonthOfTheYear;
    unsigned mMonthTimeStep;
    unsigned mYearTimeStep;

    unsigned mFirstYear;
    unsigned mLastYear;
};

#endif

