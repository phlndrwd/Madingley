#include "DispersalAdvective.h"
#include "RandomSeed.h"

DispersalAdvective::DispersalAdvective( ) {

    mTimeUnitImplementation = "month";
    mHorizontalDiffusivity = 100;
    mAdvectiveModelTimeStepLengthHours = 18;
    mHorizontalDiffusivityKmSqPerADTimeStep = mHorizontalDiffusivity / ( 1000 * 1000 ) * 60 * 60 * mAdvectiveModelTimeStepLengthHours;

    // Calculate the scalar to convert from the time step units used by this implementation of dispersal to the global model time step units
    mDeltaT = mUtilities.ConvertTimeUnits( Constants::cTimeStepUnits, mTimeUnitImplementation );

    // Initialise the advective dispersal temporal scaling to adjust between time steps appropriately
    mAdvectionTimeStepsPerModelTimeStep = mUtilities.ConvertTimeUnits( Constants::cTimeStepUnits, "day" ) * 24 / mAdvectiveModelTimeStepLengthHours;

    // Convert velocity from m/s to km/month. Note that if the _TimeUnitImplementation changes, this will also have to change.
    mVelocityUnitConversion = 60 * 60 * 24 * mUtilities.ConvertTimeUnits( Constants::cTimeStepUnits, "day" ) * mDeltaT / 1000;

    // Set the seed for the random number generator
    mRandomNumberA.SetSeed( RandomSeed::Get( )->Seed( ), RandomSeed::Get( )->Seed( ) );

}

void DispersalAdvective::Run( Grid* grid, Cohort* cohortToDisperse, const unsigned& currentMonth ) {
    // Loop through a number of times proportional to the rescaled dispersal
    for( int mm = 0; mm < mAdvectionTimeStepsPerModelTimeStep; mm++ ) {
        // Get the probability of dispersal and return a candidate cell
        CalculateDispersalProbability( grid, cohortToDisperse, currentMonth );
    }
}

inline const double DispersalAdvective::RescaleDispersalSpeed( const double& dispersalSpeed ) const {
    // FIX - Units are metres per second; need to convert to kilometres per global time step (currently one month) - use VelocityUnitConversion for this.
    // FIX - Also rescale based on the time step of the advective dispersal model - currently 18h
    return dispersalSpeed * mVelocityUnitConversion / mAdvectionTimeStepsPerModelTimeStep;
}

void DispersalAdvective::CalculateDispersalProbability( Grid* grid, Cohort* c, const unsigned& currentMonth ) {
    // Advective speed in u (longitudinal) direction
    double uSpeed;

    // Advective speed in v (latitudinal) direction
    double vSpeed;

    // Distance travelled in u (longitudinal) direction
    double uDistanceTravelled;

    // Distance travelled in v (latitudinal) direction
    double vDistanceTravelled;

    // U and V components of the diffusive velocity
    Types::DoubleVector diffusiveUandVComponents( 2 );

    // Length in km of a cell boundary latitudinally
    double cellHeight;

    // Length in km of a cell boundary longitudinally
    double cellWidth;

    // Get the u speed and the v speed from the cell data
    uSpeed = Environment::Get( "uVel", *( c->mDestinationCell ) );
    vSpeed = Environment::Get( "vVel", *( c->mDestinationCell ) );
    // Calculate the diffusive movement speed, with a direction chosen at random
    diffusiveUandVComponents = CalculateDiffusion( );
    // Calculate the distance travelled in this dispersal (not global) time step. both advective and diffusive speeds need to have been converted to km / advective model time step
    uDistanceTravelled = RescaleDispersalSpeed( uSpeed ) + diffusiveUandVComponents[ 0 ];
    vDistanceTravelled = RescaleDispersalSpeed( vSpeed ) + diffusiveUandVComponents[ 1 ];

    // Check that the u distance travelled and v distance travelled are not greater than the cell length
    cellHeight = c->mDestinationCell->GetCellHeight( );
    cellWidth = c->mDestinationCell->GetCellWidth( );

    if( uSpeed > cellWidth ) std::cout << "WARNING> DispersalAdvective: uSpeed (" << uSpeed << ") > cellWidth (" << cellWidth << ")." << std::endl;
    if( vSpeed > cellHeight ) std::cout << "WARNING> DispersalAdvective: vSpeed (" << vSpeed << ") > cellHeight (" << cellHeight << ")." << std::endl;

    NewCell( grid, uDistanceTravelled, vDistanceTravelled, cellWidth, cellHeight, c );
}

Types::DoubleVector DispersalAdvective::CalculateDiffusion( ) {
    // Create the array with which to send the output
    Types::DoubleVector outputsUandV( 2 );

    // Note that this formulation drops the delta t because we set the horizontal diffusivity to be at the same temporal
    // scale as the time step

    outputsUandV[ 0 ] = mRandomNumberA.GetNormal( ) * sqrt( ( 2.0 * mHorizontalDiffusivityKmSqPerADTimeStep ) );
    outputsUandV[ 1 ] = mRandomNumberA.GetNormal( ) * sqrt( ( 2.0 * mHorizontalDiffusivityKmSqPerADTimeStep ) );

    return outputsUandV;
}
