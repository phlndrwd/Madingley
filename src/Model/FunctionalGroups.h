#ifndef FUNCTIONALGROUPS
#define FUNCTIONALGROUPS

#include "Types.h"
#include "FunctionalGroupDefinitions.h"

class FunctionalGroups {
public:
    ~FunctionalGroups( );
    static Types::FunctionalGroupsPointer Get( );
    
    FunctionalGroupDefinitions& GetStocks( );
    FunctionalGroupDefinitions& GetCohorts( );
    
    bool SetStocks( std::string );
    bool SetCohorts( std::string );
private:
    FunctionalGroups( );
    
    static Types::FunctionalGroupsPointer mThis;
    
    /** \brief The functional group definitions of cohorts in the model */
    FunctionalGroupDefinitions mCohorts;
    /** \brief The functional group definitions of stocks in the model */
    FunctionalGroupDefinitions mStocks;
};

#endif

