#include "HarvestMarineCohorts.h"
#include "RandomSeed.h"
#include "GridCell.h"
#include "Parameters.h"
#include "FunctionalGroups.h"
#include "CatchData.h"
#include "Cohort.h"
#include "DataRecorder.h"
#include "Timing.h"

HarvestMarineCohorts::HarvestMarineCohorts( ) {
    mRandom.SetSeed( RandomSeed::Get( )->Seed( ), RandomSeed::Get( )->Seed( ) );
}

HarvestMarineCohorts::~HarvestMarineCohorts( ) {

}

void HarvestMarineCohorts::ApplyCatch( GridCell& gridCell ) {
    if( Parameters::Get( )->GetApplyCatchData( ) == true && gridCell.IsMarine( ) == true ) {
        unsigned gridCellIndex = gridCell.GetIndex( );
        double catchSumForThisCellAndYear = 0;

        // FunctionalGroup 0 is for zooplankton in the model, and "other" in the catch data. This isn't used, but as a precaution, the loop starts at FG1.
        for( unsigned functionalGroupIndex = 1; functionalGroupIndex < FunctionalGroups::Get( )->GetCohorts( ).GetNumberOfFunctionalGroups( ); ++functionalGroupIndex ) {
            
            double catchForThisGroup = CatchData::Get( )->GetTotalCatchForYearCellAndGroup( Timing::Get( )->GetTimeStep( Constants::cYearTimeUnitName ), gridCellIndex, functionalGroupIndex );
            catchForThisGroup *= Parameters::Get( )->GetCatchCoefficient( );
            DataRecorder::Get( )->AddGroupDataTo( "CatchTotalGroup", functionalGroupIndex, catchForThisGroup );
            catchSumForThisCellAndYear += catchForThisGroup;

            double realisedCatchForThisGroup = 0;
            unsigned functionalGroupPopulation = gridCell.GetFunctionalGroupPopulation( functionalGroupIndex );
            Types::BoolVector fishedCohorts( functionalGroupPopulation, false );

            while( catchForThisGroup > Parameters::Get( )->GetPlanktonSizeThreshold( ) && functionalGroupPopulation > 0 ) {
                ////////////////////////////////////////////////////////////////
                Types::UnsignedVector availableCohortIndices;
                for( unsigned index = 0; index < functionalGroupPopulation; index++ ) {
                    if( fishedCohorts[ index ] == false )
                        availableCohortIndices.push_back( index );
                }
                ////////////////////////////////////////////////////////////////
                unsigned availableIndicesSize = availableCohortIndices.size( );
                if( availableIndicesSize > 0 ) {
                    unsigned randomCohortIndex = 0;
                    if( availableIndicesSize > 1 )
                        randomCohortIndex = mRandom.GetUInt( availableIndicesSize - 1 );

                    fishedCohorts[ availableCohortIndices[ randomCohortIndex ] ] = true;
                    Types::CohortPointer cohort = gridCell.mCohorts[ functionalGroupIndex ][ availableCohortIndices[ randomCohortIndex ] ];

                    double individualMass = ( cohort->mIndividualBodyMass + cohort->mIndividualReproductivePotentialMass ) / 1000.;
                    double abundanceToRemove = catchForThisGroup / individualMass;

                    double massToRemove = 0;
                    if( abundanceToRemove < cohort->mCohortAbundance ) {
                        massToRemove = individualMass * abundanceToRemove;
                        cohort->mCohortAbundance -= abundanceToRemove;
                    } else {
                        massToRemove = individualMass * cohort->mCohortAbundance;
                        cohort->mCohortAbundance = 0; // Kill cohort
                    }
                    catchForThisGroup -= massToRemove;
                    realisedCatchForThisGroup += massToRemove;

                } else
                    break;
            }
            if( catchForThisGroup < 0 ) catchForThisGroup = 0;
            DataRecorder::Get( )->AddGroupDataTo( "CatchMissedGroup", functionalGroupIndex, catchForThisGroup );
            DataRecorder::Get( )->AddGridDataTo( "CatchMissedGrid", gridCellIndex, catchForThisGroup );
            DataRecorder::Get( )->AddGroupDataTo( "CatchAppliedGroup", functionalGroupIndex, realisedCatchForThisGroup );
            DataRecorder::Get( )->AddGridDataTo( "CatchAppliedGrid", gridCellIndex, realisedCatchForThisGroup );
        }
        DataRecorder::Get( )->AddGridDataTo( "CatchTotalGrid", gridCellIndex, catchSumForThisCellAndYear );
    }
}
