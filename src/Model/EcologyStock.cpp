#include "EcologyStock.h"
#include "FunctionalGroups.h"

void EcologyStock::RunWithinCellEcology( GridCell& gcl, Stock& actingStock, unsigned currentTimeStep, unsigned currentMonth ) {

    FunctionalGroupDefinitions& madingleyStockDefinitions = FunctionalGroups::Get( )->GetStocks( );
    //changes here to make this code consistent with the merge-and-refactor C# code
    if( gcl.IsMarine( ) ) {
        // Run the autotroph processor
        mMarineNPPtoAutotrophStock.ConvertNPPToAutotroph( gcl, actingStock );
    } else {
        // Run the dynamic plant model to update the leaf stock for this time step
        double NPPWetMatter = mDynamicPlantModel.UpdateLeafStock( gcl, actingStock, currentTimeStep, madingleyStockDefinitions.GetTraitNames( "leaf strategy", actingStock.mFunctionalGroupIndex ) == "deciduous", currentMonth );

        // Apply human appropriation of NPP - note in the latest C# version this is changed to include the NPPWetMatter calculated above
        double fhanpp = mHANPP.RemoveHumanAppropriatedMatter( NPPWetMatter, gcl, actingStock, currentTimeStep, currentMonth );
        actingStock.mTotalBiomass += NPPWetMatter * ( 1 - fhanpp );
    }
}
