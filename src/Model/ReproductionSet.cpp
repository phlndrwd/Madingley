#include "ReproductionSet.h"
#include "Reproduction.h"
#include "FunctionalGroups.h"

/**  \brief Constructor for Reproduction: fills the list of available implementations of reproduction */
ReproductionSet::ReproductionSet( ) {
    // Add the basic reproduction implementation to the list of implementations
    mImplementations["reproduction basic"] = new ReproductionBasic( );
}

/** Destructor ensure we tidy everything up */
ReproductionSet::~ReproductionSet( ) {
    delete mImplementations["reproduction basic"];
}

void ReproductionSet::InitializeEcologicalProcess( GridCell& gcl, std::string implementationKey ) {
    
}

void ReproductionSet::RunEcologicalProcess( GridCell& gcl, Cohort* actingCohort, unsigned currentTimestep, ThreadVariables& partial, unsigned currentMonth ) {
    // Holds the reproductive strategy of a cohort
    bool _Iteroparous = ( FunctionalGroups::Get( )->GetCohorts( ).GetTraitNames( "reproductive strategy", actingCohort->mFunctionalGroupIndex ) == "iteroparity" );
    // Assign mass to reproductive potential
    mImplementations["reproduction basic"]->MassAssignment( gcl, actingCohort, currentTimestep );
    // Run reproductive events. Note that we can't skip juveniles here as they could conceivably grow to adulthood and get enough biomass to reproduce in a single time step
    // due to other ecological processes
    mImplementations["reproduction basic"]->Run( gcl, actingCohort, currentTimestep, partial, _Iteroparous, currentMonth );
}
