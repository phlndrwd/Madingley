#include "Cohort.h"
#include "GridCell.h"
#include "DispersalSet.h"
#include "FunctionalGroups.h"
#include "Parameters.h"
#include "Timing.h"

#include <limits.h>

Types::Double2DMap Cohort::mMassAccounting;

// For initialisation and restart
Cohort::Cohort( GridCell& gcl, unsigned functionalGroupIndex, double juvenileBodyMass, double adultBodyMass, double initialAbundance, double logOptimalPreyBodySizeRatio, double individualReproductiveMass ) {
    mFunctionalGroupIndex = functionalGroupIndex;
    mJuvenileMass = juvenileBodyMass;
    mAdultMass = adultBodyMass;
    mIndividualBodyMass = juvenileBodyMass;
    mCohortAbundance = initialAbundance;
    mBirthTimeStep = Timing::Get( )->GetTimeStep( );
    mMaturityTimeStep = std::numeric_limits<unsigned>::max( );
    mLogOptimalPreyBodySizeRatio = logOptimalPreyBodySizeRatio;
    mMaximumAchievedBodyMass = juvenileBodyMass;
    mMerged = false;
    mProportionTimeActive = FunctionalGroups::Get( )->GetCohorts( ).GetBiologicalPropertyOneFunctionalGroup( "proportion suitable time active", functionalGroupIndex );
    mCurrentCell = &gcl;
    mDestinationCell = mCurrentCell;
    mCurrentLocation.SetIndices( gcl.GetLatitudeIndex( ), gcl.GetLongitudeIndex( ) );
    mDestinationLocation = mCurrentLocation;
    mIndividualReproductivePotentialMass = individualReproductiveMass;
    mTrophicLevel = 0;
}

// For reproduction
Cohort::Cohort( Cohort* parent, double juvenileBodyMass, double adultBodyMass, double initialBodyMass, double initialAbundance, unsigned birthTimeStep ) {
    mFunctionalGroupIndex = parent->mFunctionalGroupIndex;
    mJuvenileMass = juvenileBodyMass;
    mAdultMass = adultBodyMass;
    mIndividualBodyMass = initialBodyMass;
    mCohortAbundance = initialAbundance;
    mBirthTimeStep = birthTimeStep;
    mMaturityTimeStep = std::numeric_limits<unsigned>::max( );
    mLogOptimalPreyBodySizeRatio = parent->mLogOptimalPreyBodySizeRatio;
    mMaximumAchievedBodyMass = juvenileBodyMass;
    mMerged = false;
    mProportionTimeActive = parent->mProportionTimeActive;
    mCurrentCell = parent->mCurrentCell;
    mDestinationCell = mCurrentCell;
    mCurrentLocation = parent->mCurrentLocation;
    mDestinationLocation = mCurrentLocation;
    mIndividualReproductivePotentialMass = 0;
    mTrophicLevel = parent->mTrophicLevel;
}

bool Cohort::IsMature( ) {
    return( mMaturityTimeStep < std::numeric_limits<unsigned>::max( ) );
}

void Cohort::ResetMassFluxes( ) {
    // Initialize delta abundance sorted list with appropriate processes

    mMassAccounting["abundance"]["mortality"] = 0.0;

    // Initialize delta biomass sorted list with appropriate processes
    mMassAccounting["biomass"]["metabolism"] = 0.0;
    mMassAccounting["biomass"]["predation"] = 0.0;
    mMassAccounting["biomass"]["herbivory"] = 0.0;
    mMassAccounting["biomass"]["reproduction"] = 0.0;

    // Initialize delta reproductive biomass vector with appropriate processes

    mMassAccounting["reproductivebiomass"]["reproduction"] = 0.0;

    // Initialize organic pool delta vector with appropriate processes
    mMassAccounting["organicpool"]["herbivory"] = 0.0;
    mMassAccounting["organicpool"]["predation"] = 0.0;
    mMassAccounting["organicpool"]["mortality"] = 0.0;

    // Initialize respiratory CO2 pool delta vector with appropriate processes
    mMassAccounting["respiratoryCO2pool"]["metabolism"] = 0.0;
}

double Cohort::GetRealm( ) {
    return mCurrentCell->GetRealm( );
}

void Cohort::TryLivingAt( Types::GridCellPointer destination, Location& L ) {
    if( destination != 0 && destination->GetRealm( ) == GetRealm( ) ) {
        mDestinationCell = destination;
        mDestinationLocation = L;
    }
}

GridCell& Cohort::GetDestination( ) {
    return *mDestinationCell;
}

GridCell& Cohort::GetCurrentCell( ) {
    return *mCurrentCell;
}

void Cohort::SetCurrentCell( Types::GridCellPointer gclp ) {
    mCurrentCell = gclp;
}

bool Cohort::IsMoving( ) {
    return mCurrentCell != mDestinationCell;
}

void Cohort::Move( ) {
    mCurrentCell->MoveCohort( this );
}

bool Cohort::IsMarine( ) {
    return mCurrentCell->IsMarine( );
}

bool Cohort::IsPlanktonic( ) {
    return( IsMarine( ) && ( ( mIndividualBodyMass <= Parameters::Get( )->GetPlanktonSizeThreshold( ) ) || ( FunctionalGroups::Get( )->GetCohorts( ).GetTraitNames( "Mobility", mFunctionalGroupIndex ) == "planktonic" ) ) );
}

std::string Cohort::GetDispersalType( ) {
    std::string dispersalName;
    if( IsPlanktonic( ) ) {
        // Advective dispersal
        dispersalName = "advective";
    }// Otherwise, if mature do responsive dispersal
    else if( IsMature( ) ) {
        dispersalName = "responsive";
    }// If the cohort is immature, run diffusive dispersal
    else {
        dispersalName = "diffusive";
    }
    return dispersalName;
}
