#include "Madingley.h"
#include "FunctionalGroups.h"
#include "HarvestMarineCohorts.h"

#include <omp.h>
#include <list>

Madingley::Madingley( ) {
    mGrid = new Grid( );
    mParams = Initialisation( mGrid );
    mDispersalSet = new DispersalSet( );
}

void Madingley::Run( ) {
    // Spin up the model
    mDispersals = 0;
    unsigned numberOfSpinUpMonths = Parameters::Get( )->GetSpinUpYears( ) * 12;
    for( unsigned timeStep = 0; timeStep < numberOfSpinUpMonths; timeStep++ ) {
        Timing::Get( )->Update( timeStep % 12 );
        std::cout << "Spinning up model for t=" << timeStep + 1 << "/" << numberOfSpinUpMonths << ", " << Timing::Get( )->GetCurrentDateString( ) << "..." << std::endl;
        RunWithinCells( );
        mDispersals = RunDispersal( );
    }
    if( numberOfSpinUpMonths > 0 ) std::cout << "********************************************************************************" << std::endl;

    // Run the model
    mDispersals = 0;
    unsigned numberOfRunMonths = Parameters::Get( )->GetLengthOfSimulationInMonths( );

    for( unsigned timeStep = 0; timeStep < numberOfRunMonths; timeStep += 1 ) {
        Timing::Get( )->Update( timeStep );
        std::cout << "Running model for t=" << timeStep + 1 << "/" << numberOfRunMonths << ", i_m=" << Timing::Get( )->GetTimeStep( Constants::cMonthTimeUnitName ) << ", i_y=" << Timing::Get( )->GetTimeStep( Constants::cYearTimeUnitName ) << ", " << Timing::Get( )->GetCurrentDateString( ) << "..." << std::endl;

        // Get current time step and month
        mCurrentTimeStep = timeStep;
        mCurrentMonth = mUtilities.GetCurrentMonth( timeStep );
        mEcologyTimer.Start( );

        Environment::Update( mCurrentMonth );

        //RunWithinCellsInParallel( );
        RunWithinCells( );
        mEcologyTimer.Stop( );
        std::cout << "Within grid ecology took: " << mEcologyTimer.GetElapsedTimeSecs( ) << std::endl;

        mDispersalTimer.Start( );

        mDispersals = RunDispersal( );
        mDispersalTimer.Stop( );
        std::cout << "Across grid ecology took: " << mDispersalTimer.GetElapsedTimeSecs( ) << std::endl;

        mOutputTimer.Start( );
        unsigned numberOfCohorts = Output( );
        mOutputTimer.Stop( );
        std::cout << "Global Outputs took: " << mOutputTimer.GetElapsedTimeSecs( ) << std::endl;

        // Write the results of dispersal to the console
        std::cout << "Total Cohorts remaining " << numberOfCohorts << std::endl;
    }
}

void Madingley::RunWithinCells( ) {
    // Instantiate a class to hold thread locked global diagnostic variables
    ThreadVariables singleThreadDiagnostics( 0, 0, 0 );

    mGrid->ApplyFunctionToAllCells( [&]( GridCell & gcl ) {

        RunWithinCellStockEcology( gcl );

        RunWithinCellCohortEcology( gcl, singleThreadDiagnostics );

    } );
    // Take the results from the thread local variables and apply to the global diagnostic variables
    mGlobalDiagnosticVariables["NumberOfCohortsExtinct"] = singleThreadDiagnostics.mExtinctions - singleThreadDiagnostics.mCombinations;
    mGlobalDiagnosticVariables["NumberOfCohortsProduced"] = singleThreadDiagnostics.mProductions;
    mGlobalDiagnosticVariables["NumberOfCohortsInModel"] = mGlobalDiagnosticVariables["NumberOfCohortsInModel"] + singleThreadDiagnostics.mProductions - singleThreadDiagnostics.mExtinctions;
    mGlobalDiagnosticVariables["NumberOfCohortsCombined"] = singleThreadDiagnostics.mCombinations;
}

void Madingley::RunWithinCellsInParallel( ) {
    list<ThreadVariables> partialsDiagnostics;

#pragma omp parallel num_threads(omp_get_num_procs()) shared(partialsDiagnostics)
    {
        ThreadVariables singleThreadDiagnostics( 0, 0, 0 );

#pragma omp for schedule(dynamic)
        for( unsigned gridCellIndex = 0; gridCellIndex < Parameters::Get( )->GetNumberOfGridCells( ); gridCellIndex++ ) {
            RunWithinCellStockEcology( mGrid->GetACell( gridCellIndex ) );
            RunWithinCellCohortEcology( mGrid->GetACell( gridCellIndex ), singleThreadDiagnostics );
        }
        partialsDiagnostics.push_back( singleThreadDiagnostics );
    } //END PARALLEL REGION

    ThreadVariables globalDiagnostics( 0, 0, 0 );
    for( list<ThreadVariables>::iterator it = partialsDiagnostics.begin( ); it != partialsDiagnostics.end( ); it++ ) {
        ThreadVariables tmp = *it;
        globalDiagnostics.mProductions += tmp.mProductions;
        globalDiagnostics.mExtinctions += tmp.mExtinctions;
        globalDiagnostics.mCombinations += tmp.mCombinations;
    }

    // Take the results from the thread local variables and apply to the global diagnostic variables
    mGlobalDiagnosticVariables["NumberOfCohortsExtinct"] = globalDiagnostics.mExtinctions - globalDiagnostics.mCombinations;
    mGlobalDiagnosticVariables["NumberOfCohortsProduced"] = globalDiagnostics.mProductions;
    mGlobalDiagnosticVariables["NumberOfCohortsInModel"] = mGlobalDiagnosticVariables["NumberOfCohortsInModel"] + globalDiagnostics.mProductions - globalDiagnostics.mExtinctions;
    mGlobalDiagnosticVariables["NumberOfCohortsCombined"] = globalDiagnostics.mCombinations;
}

void Madingley::RunWithinCellStockEcology( GridCell& gcl ) {

    // Create a local instance of the stock ecology class
    EcologyStock MadingleyEcologyStock;
    // Get the list of functional group indices for autotroph stocks
    std::vector<int> AutotrophStockFunctionalGroups = FunctionalGroups::Get( )->GetStocks( ).GetFunctionalGroupIndex( "Heterotroph/Autotroph", "Autotroph", false );
    // Loop over autotroph functional groups
    for( unsigned FunctionalGroup : AutotrophStockFunctionalGroups ) {
        for( auto& ActingStock : gcl.mStocks[FunctionalGroup] ) {

            // Run stock ecology
            MadingleyEcologyStock.RunWithinCellEcology( gcl, ActingStock, mCurrentTimeStep, mCurrentMonth );
        }
    }

}

void Madingley::RunWithinCellCohortEcology( GridCell& gcl, ThreadVariables& partial ) {
    // Local instances of classes
    // Initialize ecology for stocks and cohorts - needed fresh every timestep?
    EcologyCohort mEcologyCohort;
    mEcologyCohort.InitialiseEating( gcl );
    Activity CohortActivity;
    HarvestMarineCohorts harvestMarineCohorts; // Thread-safe, local instance

    // Loop over randomly ordered gridCellCohorts to implement biological functions
    gcl.ApplyFunctionToAllCohortsWithStaticRandomness( [&]( Cohort * c ) {
        // Perform all biological functions except dispersal (which is cross grid cell)

        if( gcl.mCohorts[c->mFunctionalGroupIndex].size( ) != 0 && c->mCohortAbundance > Parameters::Get( )->GetExtinctionThreshold( ) ) {

            CohortActivity.AssignProportionTimeActive( gcl, c, mCurrentTimeStep, mCurrentMonth );

            // Run ecology
            mEcologyCohort.RunWithinCellEcology( gcl, c, mCurrentTimeStep, partial, mCurrentMonth );
            // Update the properties of the acting cohort
            mEcologyCohort.UpdateEcology( gcl, c, mCurrentTimeStep );
            Cohort::ResetMassFluxes( );
            // Check that the mass of individuals in this cohort is still >= 0 after running ecology
            assert( c->mIndividualBodyMass >= 0.0 && "Biomass < 0 for this cohort" );
        }

        // Check that the mass of individuals in this cohort is still >= 0 after running ecology
        if( gcl.mCohorts[c->mFunctionalGroupIndex].size( ) > 0 ) assert( c->mIndividualBodyMass >= 0.0 && "Biomass < 0 for this cohort" );

    }, mCurrentTimeStep );

    for( auto c : GridCell::mNewCohorts ) {
        gcl.InsertCohort( c );
        if( c->mDestinationCell != &gcl ) std::cout << "WARNING> New cohort moving in to wrong cell..." << std::endl;
    }
    partial.mProductions += GridCell::mNewCohorts.size( );
    GridCell::mNewCohorts.clear( );

    RunExtinction( gcl, partial );

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    harvestMarineCohorts.ApplyCatch( gcl );
    RunExtinction( gcl, partial );
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    // Merge cohorts, if necessary
    if( gcl.GetNumberOfCohorts( ) > Parameters::Get( )->GetMaximumNumberOfCohortsPerCell( ) ) {
        partial.mCombinations += mCohortMerger.MergeToReachThresholdFast( gcl );

        //Run extinction a second time to remove those cohorts that have been set to zero abundance when merging
        RunExtinction( gcl, partial );
    }
}

void Madingley::RunExtinction( GridCell& gcl, ThreadVariables& partial ) {

    // Loop over cohorts and remove any whose abundance is below the extinction threshold
    std::vector<Cohort*> CohortsToRemove;
    gcl.ApplyFunctionToAllCohorts( [&]( Cohort * c ) {
        if( c->mCohortAbundance - Parameters::Get( )->GetExtinctionThreshold( ) <= 0 || c->mIndividualBodyMass <= 0 ) {
            CohortsToRemove.push_back( c );
            partial.mExtinctions += 1;
        }
    } );

    // Code to add the biomass to the biomass pool and dispose of the cohort
    for( auto c : CohortsToRemove ) {

        // Add biomass of the extinct cohort to the organic matter pool
        double deadMatter = ( c->mIndividualBodyMass + c->mIndividualReproductivePotentialMass ) * c->mCohortAbundance;
        if( deadMatter < 0 ) std::cout << "Dead " << deadMatter << std::endl;
        Environment::Get( "Organic Pool", c->GetCurrentCell( ) ) += deadMatter;
        assert( Environment::Get( "Organic Pool", c->GetCurrentCell( ) ) >= 0 && "Organic pool < 0" );

        // Remove the extinct cohort from the list of cohorts
        gcl.RemoveCohort( c );
    }
    for( auto c : CohortsToRemove ) {
        delete(c );
    }
}

unsigned Madingley::RunDispersal( ) {
    // Loop through each grid cell, and run dispersal for each.
    // In the original model a new dispersal object is made every timestep - this resets the random number generators
    mDispersalSet->ResetRandoms( );
    mGrid->ApplyFunctionToAllCells( [&]( GridCell & c ) {
        mDispersalSet->RunCrossGridCellEcologicalProcess( c, mGrid, mCurrentMonth );
    } );

    // Apply the changes from dispersal
    return mDispersalSet->UpdateCrossGridCellEcology( );


    //    #pragma omp parallel num_threads( omp_get_num_procs( ) )
    //    {
    //        #pragma omp for schedule( dynamic )
    //        for( unsigned gridCellIndex = 0; gridCellIndex < Parameters::Get( )->GetNumberOfGridCells( ); gridCellIndex++ ) {
    //            Types::GridCellPointer gridCell = mModelGrid.GetCellWithIndex( gridCellIndex );
    //            gridCell->mDispersalSet->RunCrossGridCellEcologicalProcess( *gridCell, mModelGrid, mParams, mCurrentMonth );
    //
    //        }
    //    }
    //    mModelGrid.ApplyFunctionToAllCells( [ & ]( GridCell & gridCell ) {
    //        gridCell.mDispersalSet->UpdateCrossGridCellEcology( dispersals );
    //    } );
}

void Madingley::SetUpGlobalDiagnosticsList( ) {
    // Add global diagnostic variables
        mGlobalDiagnosticVariables["NumberOfCohortsExtinct"] = 0.0;
        mGlobalDiagnosticVariables["NumberOfCohortsProduced"] = 0.0;
        mGlobalDiagnosticVariables["NumberOfCohortsCombined"] = 0.0;
        mGlobalDiagnosticVariables["NumberOfCohortsInModel"] = 0.0;
        mGlobalDiagnosticVariables["NumberOfStocksInModel"] = 0.0;
}

unsigned Madingley::Output( ) {
    double totalLivingBiomass = 0.;
    double totalBiomass = 0.;

    double organicMatterPool = 0.;
    double respiratoryPool = 0.;

    double totalStockBiomass = 0.;

    double totalCohortBiomass = 0.;
    unsigned long totalCohorts = 0;
    double totalCohortAbundance = 0.;

    double meanTrophicLevel = 0.;

    unsigned numberOfFunctionalGroups = FunctionalGroups::Get( )->GetCohorts( ).GetNumberOfFunctionalGroups( );

    Types::UnsignedLongLongVector groupCohortFrequencies;
    Types::LongDoubleVector groupCohortAbundances;
    Types::LongDoubleVector groupCohortMasses;
    Types::LongDoubleVector groupTrophicLevels;

    groupCohortFrequencies.resize( numberOfFunctionalGroups, 0 );
    groupCohortAbundances.resize( numberOfFunctionalGroups, 0 );
    groupCohortMasses.resize( numberOfFunctionalGroups, 0 );
    groupTrophicLevels.resize( numberOfFunctionalGroups, 0 );

    mGrid->ApplyFunctionToAllCells( [&]( GridCell & gridCell ) {

        unsigned gridCellIndex = gridCell.GetIndex( );
        double gridCellArea = gridCell.GetCellArea( );

        unsigned cohortFrequencyThisCell = 0;

        double organicMatterThisCell = Environment::Get( "Organic Pool", gridCell ) / 1000.;
        double respirationThisCell = Environment::Get( "Respiratory CO2 Pool", gridCell ) / 1000.;

        double cohortAbundanceThisCell = 0.;
        double cohortBiomassThisCell = 0.;
        double stockBiomassThisCell = 0.;

        double phytoplanktonBiomassThisCell = 0.;
        double evergreenBiomassThisCell = 0.;
        double deciduousBiomassThisCell = 0.;

        double herbivoreBiomassThisCell = 0.;
        double herbivoreAbundanceThisCell = 0.;
        double omnivoreBiomassThisCell = 0.;
        double omnivoreAbundanceThisCell = 0.;
        double carnivoreBiomassThisCell = 0.;
        double carnivoreAbundanceThisCell = 0.;

        double ectothermBiomassThisCell = 0.;
        double ectothermAbundanceThisCell = 0.;
        double endothermBiomassThisCell = 0.;
        double endothermAbundanceThisCell = 0.;

        double iteroparousBiomassThisCell = 0.;
        double iteroparousAbundanceThisCell = 0.;
        double semelparousBiomassThisCell = 0.;
        double semelparousAbundanceThisCell = 0.;

        double meanTrophicLevelThisCell = 0.;

        organicMatterPool += organicMatterThisCell;
        respiratoryPool += respirationThisCell;

        gridCell.ApplyFunctionToAllCohorts( [&]( Cohort * cohort ) {
            double abundance = cohort->mCohortAbundance;
            double trophicLevel = cohort->mTrophicLevel;
            double cohortBiomass = ( cohort->mIndividualBodyMass + cohort->mIndividualReproductivePotentialMass ) * abundance / 1000.;

            unsigned functionalGroupIndex = cohort->mFunctionalGroupIndex;

            totalCohorts += 1;
            cohortFrequencyThisCell += 1;
            groupCohortFrequencies[ functionalGroupIndex ] += 1;

            totalCohortAbundance += abundance;
            cohortAbundanceThisCell += abundance;
            groupCohortAbundances[ functionalGroupIndex ] += abundance;

            cohortBiomassThisCell += cohortBiomass;
            totalCohortBiomass += cohortBiomass;
            groupCohortMasses[ functionalGroupIndex ] += cohortBiomass;

            meanTrophicLevel += trophicLevel;
            meanTrophicLevelThisCell += trophicLevel;
            groupTrophicLevels[ functionalGroupIndex ] += trophicLevel;

            if( FunctionalGroups::Get( )->GetCohorts( ).mTraitLookupFromIndex[ "nutrition source" ][ functionalGroupIndex ] == "herbivore" ) {
                herbivoreBiomassThisCell += cohortBiomass;
                        herbivoreAbundanceThisCell += abundance;
            } else if( FunctionalGroups::Get( )->GetCohorts( ).mTraitLookupFromIndex[ "nutrition source" ][ functionalGroupIndex ] == "omnivore" ) {
                omnivoreBiomassThisCell += cohortBiomass;
                        omnivoreAbundanceThisCell += abundance;
            } else if( FunctionalGroups::Get( )->GetCohorts( ).mTraitLookupFromIndex[ "nutrition source" ][ functionalGroupIndex ] == "carnivore" ) {
                carnivoreBiomassThisCell += cohortBiomass;
                        carnivoreAbundanceThisCell += abundance;
            }

            if( FunctionalGroups::Get( )->GetCohorts( ).mTraitLookupFromIndex[ "endo/ectotherm" ][ functionalGroupIndex ] == "endotherm" ) {
                endothermBiomassThisCell += cohortBiomass;
                        endothermAbundanceThisCell += abundance;
            } else if( FunctionalGroups::Get( )->GetCohorts( ).mTraitLookupFromIndex[ "endo/ectotherm" ][ functionalGroupIndex ] == "ectotherm" ) {
                ectothermBiomassThisCell += cohortBiomass;
                        ectothermAbundanceThisCell += abundance;
            }

            if( FunctionalGroups::Get( )->GetCohorts( ).mTraitLookupFromIndex[ "reproductive strategy" ][ functionalGroupIndex ] == "iteroparity" ) {
                iteroparousBiomassThisCell += cohortBiomass;
                        iteroparousAbundanceThisCell += abundance;
            } else if( FunctionalGroups::Get( )->GetCohorts( ).mTraitLookupFromIndex[ "reproductive strategy" ][ functionalGroupIndex ] == "semelparity" ) {
                semelparousBiomassThisCell += cohortBiomass;
                        semelparousAbundanceThisCell += abundance;
            }
        } );
        gridCell.ApplyFunctionToAllStocks( [&]( Stock & s ) {
            double thisStockBiomass = s.mTotalBiomass / 1000.;
            stockBiomassThisCell += thisStockBiomass; //convert from g to kg
            totalStockBiomass += thisStockBiomass;

            if( FunctionalGroups::Get( )->GetStocks( ).mTraitLookupFromIndex[ "leaf strategy" ][ s.mFunctionalGroupIndex ] == "na" ) phytoplanktonBiomassThisCell += thisStockBiomass;
            else if( FunctionalGroups::Get( )->GetStocks( ).mTraitLookupFromIndex[ "leaf strategy" ][ s.mFunctionalGroupIndex ] == "deciduous" ) deciduousBiomassThisCell += thisStockBiomass;
            else if( FunctionalGroups::Get( )->GetStocks( ).mTraitLookupFromIndex[ "leaf strategy" ][ s.mFunctionalGroupIndex ] == "evergreen" ) evergreenBiomassThisCell += thisStockBiomass;
            } );

        double biomassThisCell = cohortBiomassThisCell + stockBiomassThisCell + respirationThisCell + organicMatterThisCell;

        DataRecorder::Get( )->SetGridDataOn( "AutotrophBiomassDensities", gridCellIndex, stockBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "BiomassDensities", gridCellIndex, biomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "CarnivoreBiomassDensities", gridCellIndex, carnivoreBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "CarnivoreAbundanceDensities", gridCellIndex, carnivoreAbundanceThisCell / gridCellArea );
        // Catch data to go here...
        DataRecorder::Get( )->SetGridDataOn( "CohortAbundanceDensities", gridCellIndex, cohortAbundanceThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "CohortFrequencies", gridCellIndex, cohortFrequencyThisCell );
        DataRecorder::Get( )->SetGridDataOn( "DeciduousBiomassDensities", gridCellIndex, deciduousBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "EctothermBiomassDensities", gridCellIndex, ectothermBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "EctothermAbundanceDensities", gridCellIndex, ectothermAbundanceThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "EndothermBiomassDensities", gridCellIndex, endothermBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "EndothermAbundanceDensities", gridCellIndex, endothermAbundanceThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "EvergreenBiomassDensities", gridCellIndex, evergreenBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "HeterotrophBiomassDensities", gridCellIndex, cohortBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "HerbivoreBiomassDensities", gridCellIndex, herbivoreBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "HerbivoreAbundanceDensities", gridCellIndex, herbivoreAbundanceThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "IteroparityAbundanceDensities", gridCellIndex, iteroparousAbundanceThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "IteroparityBiomassDensities", gridCellIndex, iteroparousBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "MeanGridTrophicLevel", gridCellIndex, meanTrophicLevelThisCell / cohortFrequencyThisCell );
        DataRecorder::Get( )->SetGridDataOn( "OmnivoreBiomassDensities", gridCellIndex, omnivoreBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "OmnivoreAbundanceDensities", gridCellIndex, omnivoreAbundanceThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "OrganicMatter", gridCellIndex, organicMatterThisCell );
        DataRecorder::Get( )->SetGridDataOn( "PhytoplanktonBiomassDensities", gridCellIndex, phytoplanktonBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "RespiratoryCO2", gridCellIndex, respirationThisCell );
        DataRecorder::Get( )->SetGridDataOn( "SemelparityBiomassDensities", gridCellIndex, semelparousBiomassThisCell / gridCellArea );
        DataRecorder::Get( )->SetGridDataOn( "SemelparityAbundanceDensities", gridCellIndex, semelparousAbundanceThisCell / gridCellArea );
    } );
    totalLivingBiomass = totalCohortBiomass + totalStockBiomass;
    totalBiomass = totalCohortBiomass + totalStockBiomass + respiratoryPool + organicMatterPool;

    DataRecorder::Get( )->SetBasicDataOn( "CohortFrequency", totalCohorts );
    DataRecorder::Get( )->SetBasicDataOn( "CohortsProduced", mGlobalDiagnosticVariables["NumberOfCohortsProduced"] );
    DataRecorder::Get( )->SetBasicDataOn( "CohortsExtinct", mGlobalDiagnosticVariables["NumberOfCohortsExtinct"] );
    DataRecorder::Get( )->SetBasicDataOn( "CohortsCombined", mGlobalDiagnosticVariables["NumberOfCohortsCombined"] );
    DataRecorder::Get( )->SetBasicDataOn( "CohortsDispersed", mDispersals );
    DataRecorder::Get( )->SetBasicDataOn( "CohortAbundance", totalCohortAbundance );
    DataRecorder::Get( )->SetBasicDataOn( "DispersalTime", mDispersalTimer.GetElapsedTimeSecs( ) );
    DataRecorder::Get( )->SetBasicDataOn( "InCellTime", mEcologyTimer.GetElapsedTimeSecs( ) );
    DataRecorder::Get( )->SetBasicDataOn( "MeanCohortBiomass", totalCohortBiomass / totalCohortAbundance );
    DataRecorder::Get( )->SetBasicDataOn( "MeanTrophicLevel", meanTrophicLevel / totalCohorts );
    DataRecorder::Get( )->SetBasicDataOn( "OrganicMatterPool", organicMatterPool );
    DataRecorder::Get( )->SetBasicDataOn( "RespiratoryCO2Pool", respiratoryPool );
    DataRecorder::Get( )->SetBasicDataOn( "TotalCohortBiomass", totalCohortBiomass );
    DataRecorder::Get( )->SetBasicDataOn( "TotalBiomass", totalBiomass );
    DataRecorder::Get( )->SetBasicDataOn( "TotalLivingBiomass", totalLivingBiomass );
    DataRecorder::Get( )->SetBasicDataOn( "TotalStockBiomass", totalStockBiomass );

    for( unsigned functionalGroupIndex = 0; functionalGroupIndex < numberOfFunctionalGroups; ++functionalGroupIndex ) {
        long double groupAbundance = groupCohortAbundances[ functionalGroupIndex ];
        if( groupCohortFrequencies[ functionalGroupIndex ] > 0 ) {
            DataRecorder::Get( )->SetGroupDataOn( "GroupCohortFrequency", functionalGroupIndex, groupCohortFrequencies[ functionalGroupIndex ] );
            DataRecorder::Get( )->SetGroupDataOn( "TotalGroupCohortAbundance", functionalGroupIndex, groupAbundance );
            DataRecorder::Get( )->AddGroupDataTo( "TotalGroupCohortMass", functionalGroupIndex, groupCohortMasses[ functionalGroupIndex ] );
            DataRecorder::Get( )->SetGroupDataOn( "MeanGroupCohortMass", functionalGroupIndex, groupCohortMasses[ functionalGroupIndex ] / groupCohortFrequencies[ functionalGroupIndex ] );
            DataRecorder::Get( )->SetGroupDataOn( "MeanGroupTrophicLevel", functionalGroupIndex, groupTrophicLevels[ functionalGroupIndex ] / groupCohortFrequencies[ functionalGroupIndex ] );
        } else {
            DataRecorder::Get( )->SetGroupDataOn( "GroupCohortFrequency", functionalGroupIndex, Constants::cMissingValue );
            DataRecorder::Get( )->SetGroupDataOn( "TotalGroupCohortAbundance", functionalGroupIndex, Constants::cMissingValue );
            DataRecorder::Get( )->AddGroupDataTo( "TotalGroupCohortMass", functionalGroupIndex, Constants::cMissingValue );
            DataRecorder::Get( )->SetGroupDataOn( "MeanGroupCohortMass", functionalGroupIndex, Constants::cMissingValue );
            DataRecorder::Get( )->SetGroupDataOn( "MeanGroupTrophicLevel", functionalGroupIndex, Constants::cMissingValue );
        }
    }
    
    return totalCohorts;
}

Types::GridCellPointer Madingley::GetGridCell( const unsigned index ) {
    return mGrid->GetCellWithIndex( index );
}
