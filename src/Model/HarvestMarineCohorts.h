#ifndef HARVESTMARINECOHORTS
#define HARVESTMARINECOHORTS

#include "NonStaticSimpleRNG.h"
#include "GridCell.h"

class HarvestMarineCohorts {
public:
    HarvestMarineCohorts( );
    ~HarvestMarineCohorts( );
    
    void ApplyCatch( GridCell& );
private:
    
    NonStaticSimpleRNG mRandom;

};

#endif
