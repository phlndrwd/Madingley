#include "Timing.h"
#include "Date.h"
#include "Parameters.h"

Types::TimeStepPointer Timing::mThis = NULL;

Timing::Timing( ) {
    mCurrentMonthOfTheYear = 0;
    mMonthTimeStep = 0;
    mYearTimeStep = 0;

    mFirstYear = Date::GetYearOfDate( Parameters::Get( )->GetStartDateString( ) );
    mLastYear = Date::GetYearOfDate( Parameters::Get( )->GetEndDataString( ) );
}

Timing::~Timing( ) {

}

Types::TimeStepPointer Timing::Get( ) {
    if( mThis == NULL ) {
        mThis = new Timing( );
    }
    return mThis;
}

void Timing::Update( const unsigned& monthlyTimeStep ) {
    mMonthTimeStep = monthlyTimeStep;
    mCurrentMonthOfTheYear = mMonthTimeStep % 12;

    if( mMonthTimeStep >= 12 )
        mYearTimeStep = mMonthTimeStep / 12;
}

std::string Timing::GetCurrentDateString( ) {
    std::stringstream date;
    date << Constants::cMonthsOfTheYear[ mCurrentMonthOfTheYear ] << " " << mFirstYear + mYearTimeStep;
    return date.str( );
}

unsigned Timing::GetTimeStep( const std::string& timeUnit ) {
    if( timeUnit == Constants::cMonthTimeUnitName )
        return mMonthTimeStep;
    else if( timeUnit == Constants::cYearTimeUnitName )
        return mYearTimeStep;
    else
        return 0;
}

unsigned Timing::GetFirstYear( ) const {
    return mFirstYear;
}

unsigned Timing::GetLastYear( ) const {
    return mLastYear;
}
