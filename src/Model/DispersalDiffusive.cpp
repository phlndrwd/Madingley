#include "DispersalDiffusive.h"
#include "RandomSeed.h"

DispersalDiffusive::DispersalDiffusive( ) {
    mTimeUnitImplementation = "month";
    mDispersalSpeedBodyMassScalar = 0.0278;
    mDispersalSpeedBodyMassExponent = 0.48;

    // Calculate the scalar to convert from the time step units used by this implementation of dispersal to the global model time step units
    mDeltaT = mUtilities.ConvertTimeUnits( Constants::cTimeStepUnits, mTimeUnitImplementation );

    // Set the seed for the random number generator
    mRandomNumberA.SetSeed( RandomSeed::Get( )->Seed( ), RandomSeed::Get( )->Seed( ) );
}

void DispersalDiffusive::Run( Grid* grid, Cohort* cohortToDisperse, const unsigned& currentMonth ) {
    // Calculate dispersal speed for the cohort         
    double dispersalSpeed = CalculateDispersalSpeed( cohortToDisperse->mIndividualBodyMass );

    CalculateDispersalProbability( grid, cohortToDisperse, dispersalSpeed );

}

double DispersalDiffusive::CalculateDispersalSpeed( double bodyMass ) {
    return mDispersalSpeedBodyMassScalar * pow( bodyMass, mDispersalSpeedBodyMassExponent );
}

void DispersalDiffusive::CalculateDispersalProbability( Grid* grid, Cohort* cohort, double dispersalSpeed ) {
    // Check that the u speed and v speed are not greater than the cell length. If they are, then rescale them; this limits the max velocity
    // so that cohorts cannot be advected more than one grid cell per time step
    double cellHeight = cohort->mCurrentCell->GetCellHeight( );
    double cellWidth = cohort->mCurrentCell->GetCellWidth( );

    // Pick a direction at random
    double randomDirection = mRandomNumberA.GetUniform( )* 2 * acos( -1. );

    // Calculate the u and v components given the dispersal speed
    double uSpeed = dispersalSpeed * cos( randomDirection );
    double vSpeed = dispersalSpeed * sin( randomDirection );

    if( uSpeed > cellWidth ) std::cout << "WARNING> DispersalDiffusive: uSpeed (" << uSpeed << ") > cellWidth (" << cellWidth << ")." << std::endl;
    if( vSpeed > cellHeight ) std::cout << "WARNING> DispersalDiffusive: vSpeed (" << vSpeed << ") > cellHeight (" << cellHeight << ")." << std::endl;

    NewCell( grid, uSpeed, vSpeed, cellWidth, cellHeight, cohort );
}

