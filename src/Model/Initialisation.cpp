#include "Initialisation.h"
#include "FunctionalGroups.h"
#include "Timing.h"

Initialisation::Initialisation( ) {
}

Initialisation::Initialisation( Grid* grid ) {
    //read and store environmental layers
    Environment::Get( );

    grid->SetUpGrid( );

    // Set up the cohorts and stocks
    mInitializationTimer.Start( );

    long totalCohorts = 0;
    long totalStocks = 0;
    Timing::Get( )->Update( 0 ); // Required here for birth time step of Cohorts
    grid->ApplyFunctionToAllCells( [&]( GridCell& c ) {
        std::cout << "Initialising grid cell " << c.GetIndex( ) + 1 << " of " << Parameters::Get( )->GetNumberOfGridCells( ) + 1 << "..." << std::endl;
        totalCohorts += SeedGridCellCohorts( c );
        totalStocks += SeedGridCellStocks( c );
    } );

    std::cout << "Total cohorts initialised: " << totalCohorts << std::endl;
    std::cout << "Total stocks created " << totalStocks << std::endl << std::endl;

    mInitializationTimer.Stop( );
    Cohort::ResetMassFluxes( );
    std::cout << "Time required: " << mInitializationTimer.GetElapsedTimeSecs( ) << std::endl;
}

long Initialisation::SeedGridCellCohorts( GridCell& gcl ) {
    long totalCohorts = 0;
    unsigned initNumCohortsThisCell = 0;
    // Define local variables
    double cohortJuvenileMass;
    double cohortAdultMassRatio;
    double cohortAdultMass;
    double expectedLnAdultMassRatio;
    double totalNewBiomass = 0.0;
    double logOptimalPreyBodySizeRatio;

    gcl.SetCohortSize( FunctionalGroups::Get( )->GetCohorts( ).mAllFunctinoalGroupsIndex.size( ) );
    for( int FunctionalGroup : FunctionalGroups::Get( )->GetCohorts( ).mAllFunctinoalGroupsIndex ) {
        int initNumCohortsThisFGAndCell = FunctionalGroups::Get( )->GetCohorts( ).GetBiologicalPropertyOneFunctionalGroup( "Initial number of GridCellCohorts", FunctionalGroup );
        if( ( FunctionalGroups::Get( )->GetCohorts( ).GetTraitNames( "Realm", FunctionalGroup ) == "terrestrial" && !gcl.IsMarine( ) ) ||
                ( FunctionalGroups::Get( )->GetCohorts( ).GetTraitNames( "Realm", FunctionalGroup ) == "marine" && gcl.IsMarine( ) ) ) {

            initNumCohortsThisCell += initNumCohortsThisFGAndCell;
        }
    }
    if( initNumCohortsThisCell > 0 );
    {
        //Loop over all functional groups in the model
        for( int functionalGroup : FunctionalGroups::Get( )->GetCohorts( ).mAllFunctinoalGroupsIndex ) {
            // If it is a functional group that corresponds to the current realm, then seed cohorts
            if( ( FunctionalGroups::Get( )->GetCohorts( ).GetTraitNames( "Realm", functionalGroup ) == "terrestrial" && !gcl.IsMarine( ) ) ||
                    ( FunctionalGroups::Get( )->GetCohorts( ).GetTraitNames( "Realm", functionalGroup ) == "marine" && gcl.IsMarine( ) ) ) {
                // Get the minimum and maximum possible body masses for organisms in each functional group
                double massMinimum = FunctionalGroups::Get( )->GetCohorts( ).GetBiologicalPropertyOneFunctionalGroup( "minimum mass", functionalGroup );
                double massMaximum = FunctionalGroups::Get( )->GetCohorts( ).GetBiologicalPropertyOneFunctionalGroup( "maximum mass", functionalGroup );

                // Loop over the initial number of cohorts
                unsigned initNumCohortsThisFGAndCell = FunctionalGroups::Get( )->GetCohorts( ).GetBiologicalPropertyOneFunctionalGroup( "initial number of gridcellcohorts", functionalGroup );

                for( unsigned jj = 0; jj < initNumCohortsThisFGAndCell; jj++ ) {
                    mRandomNumber.SetSeed( ( uint ) ( jj + 1 ), ( uint ) ( ( jj + 1 ) * 3 ) );

                    // Draw adult mass from a log-normal distribution with mean -6.9 and standard deviation 10.0,
                    // within the bounds of the minimum and maximum body masses for the functional group

                    cohortAdultMass = pow( 10, ( mRandomNumber.GetUniform( ) * ( log10( massMaximum ) - log10( 50 * massMinimum ) ) + log10( 50 * massMinimum ) ) );
                    //Changes from original code
                    logOptimalPreyBodySizeRatio = log( std::max( 0.01, mRandomNumber.GetNormal( 0.1, 0.02 ) ) );

                    if( !gcl.IsMarine( ) ) {
                        do {
                            expectedLnAdultMassRatio = 2.24 + 0.13 * log( cohortAdultMass );

                            cohortAdultMassRatio = 1.0 + mRandomNumber.GetLogNormal( expectedLnAdultMassRatio, 0.5 );
                            cohortJuvenileMass = cohortAdultMass * 1.0 / cohortAdultMassRatio;
                        } while( cohortAdultMass <= cohortJuvenileMass || cohortJuvenileMass < massMinimum );
                    } else {
                        do {
                            expectedLnAdultMassRatio = 2.24 + 0.13 * log( cohortAdultMass );

                            cohortAdultMassRatio = 1.0 + 10 * mRandomNumber.GetLogNormal( expectedLnAdultMassRatio, 0.5 );
                            cohortJuvenileMass = cohortAdultMass * 1.0 / cohortAdultMassRatio;
                        } while( cohortAdultMass <= cohortJuvenileMass || cohortJuvenileMass < massMinimum );
                    }

                    double newBiomass = ( 3300. / initNumCohortsThisCell ) * 100 * 3000 * pow( 0.6, ( log10( cohortJuvenileMass ) ) ) * ( gcl.GetCellArea( ) );

                    totalNewBiomass += newBiomass;
                    double newAbundance = 0.0;

                    newAbundance = newBiomass / cohortJuvenileMass;

                    // Initialise the new cohort with the relevant properties
                    Cohort* cohort = new Cohort( gcl, functionalGroup, cohortJuvenileMass, cohortAdultMass, newAbundance, logOptimalPreyBodySizeRatio, 0 );

                    // Add the new cohort to the list of grid cell cohorts
                    gcl.mCohorts[functionalGroup].push_back( cohort );
                    // Increment the variable tracking the total number of cohorts in the model
                    totalCohorts++;

                }
            }
        }
    }
    return totalCohorts;
}

long Initialisation::SeedGridCellStocks( GridCell& gcl ) {

    long totalStocks = 0;

    // Loop over all stock functional groups in the model
    for( int functionalGroupIndex : FunctionalGroups::Get( )->GetStocks( ).mAllFunctinoalGroupsIndex ) {
        // Initialise the new stock with the relevant properties
        bool success;
        Stock NewStock( FunctionalGroups::Get( )->GetStocks( ), functionalGroupIndex, gcl, success );
        // Add the new stock to the list of grid cell stocks
        if( success ) {
            gcl.mStocks[functionalGroupIndex].push_back( NewStock );

            totalStocks++;
        }
    }

    return totalStocks;
}

