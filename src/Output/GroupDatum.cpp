#include "GroupDatum.h"

#include "Constants.h"
#include "Types.h"
#include "Parameters.h"
#include "Timing.h"
#include "FunctionalGroups.h"

GroupDatum::GroupDatum( const std::string& name, const std::string& timeUnit, const std::string& dataUnit ): Datum( name, timeUnit, dataUnit ) {
    Initialise( );
}

void GroupDatum::Initialise( ) {
    unsigned size = FunctionalGroups::Get( )->GetCohorts( ).GetNumberOfFunctionalGroups( ) * Parameters::Get( )->GetLengthOfSimulationInMonths( );

    if( mTimeUnit == Constants::cYearTimeUnitName )
        size = FunctionalGroups::Get( )->GetCohorts( ).GetNumberOfFunctionalGroups( ) * Parameters::Get( )->GetLengthOfSimulationInYears( );

    mData = new float[ size ]( );
}

float GroupDatum::GetDatum( const unsigned& functionalGroupIndex ) {
    unsigned dataIndex = Timing::Get( )->GetTimeStep( mTimeUnit ) + ( functionalGroupIndex * Parameters::Get( )->GetLengthOfSimulationInMonths( ) );
    return mData[ dataIndex ];
}

void GroupDatum::SetDatum( const unsigned& functionalGroupIndex, const float& data ) {
    unsigned dataIndex = Timing::Get( )->GetTimeStep( mTimeUnit ) + ( functionalGroupIndex * Parameters::Get( )->GetLengthOfSimulationInMonths( ) );
    mData[ dataIndex ] = data;
}

void GroupDatum::AddToDatum( const unsigned& functionalGroupIndex, const float& data ) {
    unsigned dataIndex = 0;
    if( mTimeUnit == Constants::cMonthTimeUnitName )
        dataIndex = Timing::Get( )->GetTimeStep( mTimeUnit ) + ( functionalGroupIndex * Parameters::Get( )->GetLengthOfSimulationInMonths( ) );
    else if( mTimeUnit == Constants::cYearTimeUnitName )
        dataIndex = Timing::Get( )->GetTimeStep( mTimeUnit ) + ( functionalGroupIndex * Parameters::Get( )->GetLengthOfSimulationInYears( ) );

    mData[ dataIndex ] += data;
}