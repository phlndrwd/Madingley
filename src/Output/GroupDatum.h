#ifndef GROUPDATUM
#define	GROUPDATUM

#include "Types.h"
#include "Datum.h"

class GroupDatum : public Datum {
public:
    GroupDatum( const std::string&, const std::string&, const std::string& );

    void Initialise( );
    
    float GetDatum( const unsigned& );
    void SetDatum( const unsigned&, const float& );
    void AddToDatum( const unsigned&, const float& );
    
private:
};

#endif

