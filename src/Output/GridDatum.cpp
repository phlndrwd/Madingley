#include "GridDatum.h"

#include "Constants.h"
#include "Types.h"
#include "Parameters.h"
#include "Timing.h"

GridDatum::GridDatum( const std::string& name, const std::string& timeUnit, const std::string& dataUnit ): Datum( name, timeUnit, dataUnit ) {
    Initialise( );
}

void GridDatum::Initialise( ) {
    unsigned size = Parameters::Get( )->GetSizeOfMonthlyGridDatum( );

    if( mTimeUnit == Constants::cNoTimeUnitName )
        size = Parameters::Get( )->GetNumberOfGridCells( );
    else if( mTimeUnit == Constants::cYearTimeUnitName )
        size = Parameters::Get( )->GetSizeOfAnnualGridDatum( );

    mData = new float[ size ]( );
}

float GridDatum::GetDatum( const unsigned& cellIndex ) {
    unsigned dataIndex = cellIndex + ( Timing::Get( )->GetTimeStep( mTimeUnit ) * Parameters::Get( )->GetNumberOfGridCells( ) );
    return mData[ dataIndex ];
}

void GridDatum::SetDatum( const unsigned& cellIndex, const float& data ) {
    unsigned dataIndex = cellIndex + ( Timing::Get( )->GetTimeStep( mTimeUnit ) * Parameters::Get( )->GetNumberOfGridCells( ) );
    mData[ dataIndex ] = data;
}

void GridDatum::AddToDatum( const unsigned& cellIndex, const float& data ) {
    unsigned dataIndex = cellIndex + ( Timing::Get( )->GetTimeStep( mTimeUnit ) * Parameters::Get( )->GetNumberOfGridCells( ) );
    mData[ dataIndex ] += data;
}