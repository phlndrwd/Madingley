#include "DataRecorder.h"

#include "Constants.h"
#include "BasicDatum.h"
#include "GridDatum.h"
#include "GroupDatum.h"
#include "Strings.h"
#include "Parameters.h"
#include "FunctionalGroups.h"

Types::DataRecorderPointer DataRecorder::mThis = NULL;

Types::DataRecorderPointer DataRecorder::Get( ) {
    if( mThis == NULL ) {
        mThis = new DataRecorder( );
    }
    return mThis;
}

DataRecorder::~DataRecorder( ) {
    for( Types::BasicDatumMap::iterator iter = mBasicDatumMap.begin( ); iter != mBasicDatumMap.end( ); iter++ ) {
        delete iter->second;
    }
    for( Types::GridDatumMap::iterator iter = mGridDatumMap.begin( ); iter != mGridDatumMap.end( ); iter++ ) {
        delete iter->second;
    }
    for( Types::GroupDatumMap::iterator iter = mGroupDatumMap.begin( ); iter != mGroupDatumMap.end( ); iter++ ) {
        delete iter->second;
    }
    if( mThis != NULL ) {
        delete mThis;
    }
}

DataRecorder::DataRecorder( ) {

}

bool DataRecorder::Initialise( const Types::StringMatrix& rawOutputParameterData ) {
    bool success = false;
    if( rawOutputParameterData.size( ) > 0 ) {
        if( rawOutputParameterData[ 0 ].size( ) == Constants::eDataUnit + 1 ) {
            for( unsigned rowIndex = 0; rowIndex < rawOutputParameterData.size( ); rowIndex++ ) {
                std::string name = rawOutputParameterData[ rowIndex ][ Constants::eDatumName ];
                std::string type = Strings::Get( )->ToLowercase( rawOutputParameterData[ rowIndex ][ Constants::eDatumType ] );
                std::string timeUnit = Strings::Get( )->ToLowercase( rawOutputParameterData[ rowIndex ][ Constants::eTimeUnit ] );
                std::string dataUnit = Strings::Get( )->ToLowercase( rawOutputParameterData[ rowIndex ][ Constants::eDataUnit ] );

                Types::StringVector datumMetadata;

                datumMetadata.push_back( name );
                datumMetadata.push_back( type );
                datumMetadata.push_back( timeUnit );
                datumMetadata.push_back( dataUnit );

                if( type == Constants::cBasicDatumTypeName && ( timeUnit == Constants::cMonthTimeUnitName || timeUnit == Constants::cYearTimeUnitName ) ) {
                    mBasicOutputMetadata.push_back( datumMetadata );
                } else if( type == Constants::cGridDatumTypeName && ( timeUnit == Constants::cMonthTimeUnitName || timeUnit == Constants::cYearTimeUnitName || timeUnit == Constants::cNoTimeUnitName ) ) {
                    mGridOutputMetadata.push_back( datumMetadata );
                } else if( type == Constants::cGroupDatumTypeName && ( timeUnit == Constants::cMonthTimeUnitName || timeUnit == Constants::cYearTimeUnitName ) ) {
                    mGroupOutputMetadata.push_back( datumMetadata );
                } else {
                    std::cout << "ERROR> Output datum \"" << name << "\" specification invalid. System exiting..." << std::endl;
                    exit( 1 );
                }
            }
            success = true;
        }
    }
    return success;
}

float DataRecorder::GetBasicData( const std::string& name ) {
    Types::BasicDatumPointer basicDatum = GetBasicDatum( name );

    if( basicDatum != NULL )
        return basicDatum->GetDatum( );
    else
        return Constants::cMissingValue;
}

float DataRecorder::GetGridData( const std::string& name, const unsigned& cellIndex ) {
    Types::GridDatumPointer gridDatum = GetGridDatum( name );

    if( gridDatum != NULL )
        return gridDatum->GetDatum( cellIndex );
    else
        return Constants::cMissingValue;
}

float DataRecorder::GetGroupData( const unsigned& functionalGroupIndex, const std::string& name ) {
    Types::GroupDatumPointer groupDatum = GetGroupDatum( name );

    if( groupDatum != NULL )
        return groupDatum->GetDatum( functionalGroupIndex );
    else
        return Constants::cMissingValue;
}

void DataRecorder::SetBasicDataOn( const std::string& name, const float& data ) {
    Types::BasicDatumPointer basicDatum = GetBasicDatum( name );

    if( basicDatum != NULL )
        basicDatum->SetDatum( data );
}

void DataRecorder::AddBasicDataTo( const std::string& name, const float& data ) {
    Types::BasicDatumPointer basicDatum = GetBasicDatum( name );

    if( basicDatum != NULL )
        basicDatum->AddToDatum( data );
}

void DataRecorder::SetGridDataOn( const std::string& name, const unsigned& cellIndex, const float& data ) {
    Types::GridDatumPointer gridDatum = GetGridDatum( name );

    if( gridDatum != NULL )
        gridDatum->SetDatum( cellIndex, data );
}

void DataRecorder::AddGridDataTo( const std::string& name, const unsigned& cellIndex, const float& data ) {
    Types::GridDatumPointer gridDatum = GetGridDatum( name );

    if( gridDatum != NULL )
        gridDatum->AddToDatum( cellIndex, data );
}

void DataRecorder::SetGroupDataOn( const std::string& name, const unsigned& functionalGroupIndex, const float& data ) {
    Types::GroupDatumPointer groupDatum = GetGroupDatum( name );

    if( groupDatum != NULL )
        groupDatum->SetDatum( functionalGroupIndex, data );
}

void DataRecorder::AddGroupDataTo( const std::string& name, const unsigned& functionalGroupIndex, const float& data ) {
    Types::GroupDatumPointer groupDatum = GetGroupDatum( name );

    if( groupDatum != NULL )
        groupDatum->AddToDatum( functionalGroupIndex, data );
}

Types::BasicDatumMap DataRecorder::GetBasicDatumMap( ) const {
    return mBasicDatumMap;
}

Types::GridDatumMap DataRecorder::GetGridDatumMap( ) const {
    return mGridDatumMap;
}

Types::GroupDatumMap DataRecorder::GetGroupDatumMap( ) const {
    return mGroupDatumMap;
}

Types::StringVector DataRecorder::GetInputFilePaths( ) const {
    return mInputFilePaths;
}

void DataRecorder::AddInputFilePath( const std::string& inputFilePath ) {
    mInputFilePaths.push_back( inputFilePath );
}

void DataRecorder::ClearInputFilePaths( ) {
    mInputFilePaths.clear( );
}

Types::BasicDatumPointer DataRecorder::GetBasicDatum( const std::string& name ) {
    Types::BasicDatumPointer basicDatum = NULL;
    Types::BasicDatumMap::iterator iter = mBasicDatumMap.find( name );

    if( iter != mBasicDatumMap.end( ) ) {
        basicDatum = iter->second;
    } else {
        for( unsigned index = 0; index < mBasicOutputMetadata.size( ); index++ ) {
            std::string datumName = mBasicOutputMetadata[ index ][ Constants::eDatumName ];

            if( Strings::Get( )->ToLowercase( datumName ) == Strings::Get( )->ToLowercase( name ) ) {
                basicDatum = new BasicDatum( datumName, mBasicOutputMetadata[ index ][ Constants::eTimeUnit ], mBasicOutputMetadata[ index ][ Constants::eDataUnit ] );
                mBasicDatumMap.insert( std::pair< std::string, Types::BasicDatumPointer >( datumName, basicDatum ) );
                break;
            }
        }
    }
    return basicDatum;
}

Types::GridDatumPointer DataRecorder::GetGridDatum( const std::string& name ) {
    Types::GridDatumPointer datum = NULL;
    Types::GridDatumMap::iterator iter = mGridDatumMap.find( name );

    if( iter != mGridDatumMap.end( ) ) {
        datum = iter->second;
    } else {
        for( unsigned index = 0; index < mGridOutputMetadata.size( ); index++ ) {
            std::string datumName = mGridOutputMetadata[ index ][ Constants::eDatumName ];

            if( Strings::Get( )->ToLowercase( datumName ) == Strings::Get( )->ToLowercase( name ) ) {
                datum = new GridDatum( datumName, mGridOutputMetadata[ index ][ Constants::eTimeUnit ], mGridOutputMetadata[ index ][ Constants::eDataUnit ] );
                mGridDatumMap.insert( std::pair< std::string, Types::GridDatumPointer >( datumName, datum ) );
                break;
            }
        }
    }
    return datum;
}

Types::GroupDatumPointer DataRecorder::GetGroupDatum( const std::string& name ) {
    Types::GroupDatumPointer datum = NULL;
    Types::GroupDatumMap::iterator iter = mGroupDatumMap.find( name );

    if( iter != mGroupDatumMap.end( ) ) {
        datum = iter->second;
    } else {
        for( unsigned index = 0; index < mGroupOutputMetadata.size( ); index++ ) {
            std::string datumName = mGroupOutputMetadata[ index ][ Constants::eDatumName ];

            if( Strings::Get( )->ToLowercase( datumName ) == Strings::Get( )->ToLowercase( name ) ) {
                datum = new GroupDatum( datumName, mGroupOutputMetadata[ index ][ Constants::eTimeUnit ], mGroupOutputMetadata[ index ][ Constants::eDataUnit ] );
                mGroupDatumMap.insert( std::pair< std::string, Types::GroupDatumPointer >( datumName, datum ) );
                break;
            }
        }
    }
    return datum;
}
