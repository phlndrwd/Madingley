#ifndef DATUM
#define DATUM

#include "Types.h"

class Datum {
public:
    Datum( const std::string&, const std::string&, const std::string& );
    ~Datum( );
    
    virtual void Initialise( ) = 0;
    
    std::string GetName( ) const;
    std::string GetTimeUnit( ) const;
    std::string GetDataUnit( ) const;
    float* GetData( ) const;
    
protected:
    std::string mName;
    std::string mTimeUnit;
    std::string mDataUnit;
    float* mData;
};

#endif

