#include "BasicDatum.h"

#include "Constants.h"
#include "Parameters.h"
#include "Timing.h"

BasicDatum::BasicDatum( const std::string& name, const std::string& timeUnit, const std::string& dataUnit ): Datum( name, timeUnit, dataUnit ) {
    Initialise( );
}

void BasicDatum::Initialise( ) {
    unsigned size = Parameters::Get( )->GetLengthOfSimulationInMonths( );

    if( mTimeUnit == Constants::cYearTimeUnitName )
        size = Parameters::Get( )->GetLengthOfSimulationInYears( );

    mData = new float[ size ]( );
}

float BasicDatum::GetDatum( ) {
    return mData[ Timing::Get( )->GetTimeStep( mTimeUnit ) ];
}

void BasicDatum::SetDatum( const float& data ) {
    mData[ Timing::Get( )->GetTimeStep( mTimeUnit ) ] = data;
}

void BasicDatum::AddToDatum( const float& data ) {
    mData[ Timing::Get( )->GetTimeStep( mTimeUnit ) ] += data;
}