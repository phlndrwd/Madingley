#include "Datum.h"

#include "Constants.h"
#include "Types.h"
#include "Parameters.h"
#include "Timing.h"

Datum::Datum( const std::string& name, const std::string& timeUnit, const std::string& dataUnit ) {
    mName = name;
    mTimeUnit = timeUnit;
    mDataUnit = dataUnit;
}

Datum::~Datum( ) {
    delete[ ] mData;
}

std::string Datum::GetName( ) const {
    return mName;
}

std::string Datum::GetTimeUnit( ) const {
    return mTimeUnit;
}

std::string Datum::GetDataUnit( ) const {
    return mDataUnit;
}

float* Datum::GetData( ) const {
    return mData;
}
