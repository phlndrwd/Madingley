#include "FileWriter.h"
#include "Constants.h"
#include "Date.h"
#include "Strings.h"
#include "Parameters.h"
#include "DataRecorder.h"
#include "BasicDatum.h"
#include "GridDatum.h"
#include "GroupDatum.h"
#include "RandomSeed.h"
#include "Madingley.h"
#include "DataCoords.h"
#include "FunctionalGroups.h"

FileWriter::FileWriter( ) {
    bool success = false;
    mTimeAndDateString = "";
    if( InitialiseOutputDirectory( ) )
        if( WriteInputFiles( ) )
            success = true;

    if( success )
        std::cout << "Output directory \"" << mOutputDirectory << "\" initialised successfully..." << std::endl;
    else {
        std::cout << "ERROR> Unable to initialise output directory. Could not access \"" << mOutputDirectory << "\"." << std::endl;
        exit( 1 );
    }
}

FileWriter::~FileWriter( ) {

}

void FileWriter::WriteFiles( Madingley& madingley ) {
    bool success = false;
    // Ensures that data gets written if output directory is deleted.
    mkdir( mOutputDirectory.c_str( ), Constants::cOutputFolderPermissions );
    if( WriteBasicOutputs( ) )
        if( WriteGridOutputs( ) )
            if( WriteGroupOutputs( ) )
                success = WriteStateFile( madingley );

    if( success )
        std::cout << "Files written to \"" << mOutputDirectory << "\" successfully..." << std::endl;
    else {
        std::cout << "ERROR> File writing failed. Could not access \"" << mOutputDirectory << "\"." << std::endl;
        exit( 1 );
    }
}

std::string & FileWriter::GetOutputDirectory( ) {
    return mOutputDirectory;
}

bool FileWriter::InitialiseOutputDirectory( ) {
    if( mTimeAndDateString == "" ) mTimeAndDateString = Date::GetDateAndTimeString( );

    // Create base directory: ./output/
    mOutputDirectory = Constants::cOutputBaseDirectory;
    int returnValue = mkdir( mOutputDirectory.c_str( ), Constants::cOutputFolderPermissions );
    if( returnValue != -1 && returnValue != 0 )
        return false;

    // Create experiment directory (if defined): ./output/experiment/
    std::string experimentName = Parameters::Get( )->GetExperimentName( );
    if( experimentName[ 0 ] != ' ' ) {
        mOutputDirectory.append( experimentName );
        int returnValue = mkdir( mOutputDirectory.c_str( ), Constants::cOutputFolderPermissions );
        if( returnValue != -1 && returnValue != 0 ) return false;
    }
    mOutputDirectory.append( Strings::Get( )->ToString( Constants::cFolderDelimiter ) );

    // Create time and date output directory: ./output/experiment/outputs/
    mOutputDirectory.append( mTimeAndDateString );
    returnValue = mkdir( mOutputDirectory.c_str( ), Constants::cOutputFolderPermissions );
    if( returnValue == -1 ) {
        mOutputDirectory.append( "_" );
        int stringLength = mOutputDirectory.length( );
        unsigned count = 1;
        while( returnValue == -1 ) {
            mOutputDirectory.replace( stringLength, 1, Strings::Get( )->ToString( count ) );
            returnValue = mkdir( mOutputDirectory.c_str( ), Constants::cOutputFolderPermissions );
            ++count;
        }
    } else if( returnValue != 0 )
        return false;

    mOutputDirectory.append( Strings::Get( )->ToString( Constants::cFolderDelimiter ) );

    return true;
}

bool FileWriter::WriteInputFiles( ) const {
    Types::StringVector inputFilePaths = DataRecorder::Get( )->GetInputFilePaths( );

    for( unsigned stringIndex = 0; stringIndex < inputFilePaths.size( ); stringIndex++ ) {
        std::ifstream sourceFileStream( inputFilePaths[ stringIndex ].c_str( ), std::ios::in );

        if( sourceFileStream.is_open( ) ) {
            std::string outputFilePath = mOutputDirectory;
            Types::StringVector inputFilePathComponents = Strings::Get( )->StringToWords( inputFilePaths[ stringIndex ], Constants::cFolderDelimiter );

            std::string fileName = inputFilePathComponents[ inputFilePathComponents.size( ) - 1 ];
            outputFilePath.append( fileName );

            std::ofstream destinationFileStream( outputFilePath.c_str( ), std::ios::out );
            if( destinationFileStream.is_open( ) ) {
                destinationFileStream << sourceFileStream.rdbuf( );

                sourceFileStream.close( );
                destinationFileStream.close( );
            } else
                return false;
        } else
            return false;
    }
    DataRecorder::Get( )->ClearInputFilePaths( );
    return true;
}

bool FileWriter::WriteBasicOutputs( ) const {
    Types::BasicDatumMap basicDatumMap = DataRecorder::Get( )->GetBasicDatumMap( );

    // Separate monthly and annual basic datums
    if( basicDatumMap.size( ) > 0 ) {
        Types::BasicDatumVector annualBasicDatumVector;
        Types::BasicDatumVector monthlyBasicDatumVector;

        for( Types::BasicDatumMap::iterator iter = basicDatumMap.begin( ); iter != basicDatumMap.end( ); iter++ ) {
            if( iter->second->GetTimeUnit( ) == Constants::cYearTimeUnitName ) {
                annualBasicDatumVector.push_back( iter->second );
            } else if( iter->second->GetTimeUnit( ) == Constants::cMonthTimeUnitName ) {
                monthlyBasicDatumVector.push_back( iter->second );
            }
        }

        // Write annual basic datums
        if( WriteBasicDatums( annualBasicDatumVector, Constants::eYear ) )
            // Write monthly basic datums
            return WriteBasicDatums( monthlyBasicDatumVector, Constants::eMonth );
    } else {
        return false;
    }
}

bool FileWriter::WriteGridOutputs( ) const {
    Types::GridDatumMap gridDatumMap = DataRecorder::Get( )->GetGridDatumMap( );

    // Separate monthly and annual grid datums
    if( gridDatumMap.size( ) > 0 ) {
        Types::GridDatumVector annualGridDatumVector;
        Types::GridDatumVector monthlyGridDatumVector;
        Types::GridDatumVector noTimeGridDatumVector;

        for( Types::GridDatumMap::iterator iter = gridDatumMap.begin( ); iter != gridDatumMap.end( ); iter++ ) {
            if( iter->second->GetTimeUnit( ) == Constants::cYearTimeUnitName )
                annualGridDatumVector.push_back( iter->second );
            else if( iter->second->GetTimeUnit( ) == Constants::cMonthTimeUnitName )
                monthlyGridDatumVector.push_back( iter->second );
            else if( iter->second->GetTimeUnit( ) == Constants::cNoTimeUnitName )
                noTimeGridDatumVector.push_back( iter->second );
        }
        // Write annual grid datums
        if( WriteGridDatums( annualGridDatumVector, Constants::eYear ) )
            // Write monthly grid datums
            if( WriteGridDatums( monthlyGridDatumVector, Constants::eMonth ) )
                // Write timeless grid datums
                return WriteGridDatums( noTimeGridDatumVector, Constants::eNoTime );
    } else {
        return false;
    }
}

bool FileWriter::WriteGroupOutputs( ) const {
    Types::GroupDatumMap groupDatumMap = DataRecorder::Get( )->GetGroupDatumMap( );

    // Separate monthly and annual group datums
    if( groupDatumMap.size( ) > 0 ) {
        Types::GroupDatumVector annualGroupDatumVector;
        Types::GroupDatumVector monthlyGroupDatumVector;

        for( Types::GroupDatumMap::iterator iter = groupDatumMap.begin( ); iter != groupDatumMap.end( ); iter++ ) {
            if( iter->second->GetTimeUnit( ) == Constants::cYearTimeUnitName ) {
                annualGroupDatumVector.push_back( iter->second );
            } else if( iter->second->GetTimeUnit( ) == Constants::cMonthTimeUnitName ) {
                monthlyGroupDatumVector.push_back( iter->second );
            }
        }

        // Write annual basic datums
        if( WriteGroupDatums( annualGroupDatumVector, Constants::eYear ) )
            // Write monthly basic datums
            return WriteGroupDatums( monthlyGroupDatumVector, Constants::eMonth );
    }
    return true;
}

bool FileWriter::WriteStateFile( Madingley& madingley ) const {
    if( Parameters::Get( )->GetWriteModelState( ) == true ) {
        std::string fileName = mOutputDirectory;
        fileName.append( Constants::cStateFileName );

        std::ofstream modelStateFileStream;
        modelStateFileStream.open( fileName.c_str( ), std::ios::out );

        modelStateFileStream.flags( std::ios::scientific );
        modelStateFileStream.precision( std::numeric_limits< double >::digits10 );

        if( modelStateFileStream.is_open( ) == true ) {
            // Header (for consistency with general file reading function)
            modelStateFileStream << Constants::cStateFileName << std::endl;
            // Write grid attributes for initial compatibility check when reading
            modelStateFileStream << Parameters::Get( )->GetMaximumNumberOfCohortsPerCell( ) << std::endl;
            modelStateFileStream << Parameters::Get( )->GetLengthUserLongitudeArray( ) << std::endl;
            modelStateFileStream << Parameters::Get( )->GetLengthUserLatitudeArray( ) << std::endl;
            // Model variables
            for( unsigned cellIndex = 0; cellIndex < Parameters::Get( )->GetNumberOfGridCells( ); cellIndex++ ) {
                modelStateFileStream << Constants::cGridCellString << Constants::cDataDelimiterValue << cellIndex << std::endl;
                Types::GridCellPointer gridCell = madingley.GetGridCell( cellIndex );

                gridCell->ApplyFunctionToAllStocks( [&]( Stock & stock ) {
                    modelStateFileStream << Constants::cStockString << Constants::cDataDelimiterValue << stock.mFunctionalGroupIndex << Constants::cDataDelimiterValue << stock.mTotalBiomass << std::endl;
                } );
                
                gridCell->ApplyFunctionToAllCohorts( [&]( Types::CohortPointer cohort ) {
                    modelStateFileStream << Constants::cCohortString << Constants::cDataDelimiterValue << cohort->mFunctionalGroupIndex << Constants::cDataDelimiterValue << cohort->mJuvenileMass << Constants::cDataDelimiterValue << cohort->mAdultMass << Constants::cDataDelimiterValue << cohort->mCohortAbundance << Constants::cDataDelimiterValue << cohort->mLogOptimalPreyBodySizeRatio << Constants::cDataDelimiterValue << cohort->mIndividualReproductivePotentialMass << std::endl;
                } );
            }
            modelStateFileStream.close( );
        } else
            return false;
    }
    return true;
}

bool FileWriter::WriteBasicDatums( Types::BasicDatumVector& basicDatumVector, const unsigned temporalResolution ) const {
    if( basicDatumVector.size( ) > 0 ) {
        std::string filePath;
        unsigned* timeArray;
        unsigned lengthOfTime;

        if( temporalResolution == Constants::eYear ) {
            filePath = Constants::cAnnualBasicOutputsFileName;
            timeArray = Parameters::Get( )->GetYearArray( );
            lengthOfTime = Parameters::Get( )->GetLengthOfSimulationInYears( );
        } else if( temporalResolution == Constants::eMonth ) {
            filePath = Constants::cMonthlyBasicOutputsFileName;
            timeArray = Parameters::Get( )->GetTimeStepArray( );
            lengthOfTime = Parameters::Get( )->GetLengthOfSimulationInMonths( );
        } else
            return false;

        filePath.insert( 0, mOutputDirectory );
        try {
            netCDF::NcFile basicOutputsNcFile( filePath, netCDF::NcFile::replace ); // Creates file
            netCDF::NcDim timeNcDim = basicOutputsNcFile.addDim( Constants::cTimeVariableNames[ 0 ], lengthOfTime ); // Creates dimension
            netCDF::NcVar timeNcVar = basicOutputsNcFile.addVar( Constants::cTimeVariableNames[ 0 ], netCDF::ncFloat, timeNcDim ); // Creates variable
            timeNcVar.putVar( timeArray );
            timeNcVar.putAtt( Constants::cUnitsString, basicDatumVector[ 0 ]->GetTimeUnit( ) );

            Types::NcDimVector dataDimensions;
            dataDimensions.push_back( timeNcDim );

            for( Types::BasicDatumVector::iterator iter = basicDatumVector.begin( ); iter != basicDatumVector.end( ); iter++ ) {
                netCDF::NcVar basicDatumNcVar = basicOutputsNcFile.addVar( ( *iter )->GetName( ), netCDF::ncFloat, dataDimensions );
                basicDatumNcVar.putAtt( Constants::cUnitsString, ( *iter )->GetDataUnit( ) );
                basicDatumNcVar.putVar( ( *iter )->GetData( ) );
            }
        } catch( netCDF::exceptions::NcException& e ) {
            e.what( );
            return false;
        }
    }
    return true;
}

bool FileWriter::WriteGridDatums( Types::GridDatumVector& gridDatumVector, const unsigned temporalResolution ) const {
    if( gridDatumVector.size( ) > 0 ) {
        std::string filePath;
        unsigned* timeArray;
        unsigned lengthOfTime;

        if( temporalResolution == Constants::eYear ) {
            filePath = Constants::cAnnualGridOutputsFileName;
            timeArray = Parameters::Get( )->GetYearArray( );
            lengthOfTime = Parameters::Get( )->GetLengthOfSimulationInYears( );
        } else if( temporalResolution == Constants::eMonth ) {
            filePath = Constants::cMonthlyGridOutputsFileName;
            timeArray = Parameters::Get( )->GetTimeStepArray( );
            lengthOfTime = Parameters::Get( )->GetLengthOfSimulationInMonths( );
        } else if( temporalResolution == Constants::eNoTime )
            filePath = Constants::cNoTimeGridOutputsFileName;
        else
            return false;

        filePath.insert( 0, mOutputDirectory );
        try {
            netCDF::NcFile gridOutputsNcFile( filePath, netCDF::NcFile::replace ); // Creates file

            Types::NcDimVector dataDimensions;

            netCDF::NcDim longitudeDim = gridOutputsNcFile.addDim( Constants::cLongitudeVariableNames[ 0 ], Parameters::Get( )->GetLengthUserLongitudeArray( ) );
            netCDF::NcVar longitudeNcVar = gridOutputsNcFile.addVar( Constants::cLongitudeVariableNames[ 0 ], netCDF::ncFloat, longitudeDim );
            longitudeNcVar.putVar( Parameters::Get( )->GetUserLongitudeArray( ) );
            longitudeNcVar.putAtt( Constants::cUnitsString, Constants::cLongitudeUnitName );

            netCDF::NcDim latitudeDim = gridOutputsNcFile.addDim( Constants::cLatitudeVariableNames[ 0 ], Parameters::Get( )->GetLengthUserLatitudeArray( ) );
            netCDF::NcVar latitudeNcVar = gridOutputsNcFile.addVar( Constants::cLatitudeVariableNames[ 0 ], netCDF::ncFloat, latitudeDim );
            latitudeNcVar.putVar( Parameters::Get( )->GetUserLatitudeArray( ) );
            latitudeNcVar.putAtt( Constants::cUnitsString, Constants::cLatitudeUnitName );

            if( temporalResolution != Constants::eNoTime ) {
                netCDF::NcDim timeNcDim = gridOutputsNcFile.addDim( Constants::cTimeVariableNames[ 0 ], lengthOfTime );
                netCDF::NcVar timeNcVar = gridOutputsNcFile.addVar( Constants::cTimeVariableNames[ 0 ], netCDF::ncFloat, timeNcDim );
                timeNcVar.putVar( timeArray );
                timeNcVar.putAtt( Constants::cUnitsString, gridDatumVector[ 0 ]->GetTimeUnit( ) );
                dataDimensions.push_back( timeNcDim );
            }
            dataDimensions.push_back( latitudeDim );
            dataDimensions.push_back( longitudeDim );

            for( Types::GridDatumVector::iterator iter = gridDatumVector.begin( ); iter != gridDatumVector.end( ); iter++ ) {
                netCDF::NcVar gridDatumNcVar = gridOutputsNcFile.addVar( ( *iter )->GetName( ), netCDF::ncFloat, dataDimensions );
                gridDatumNcVar.putAtt( Constants::cUnitsString, ( *iter )->GetDataUnit( ) );
                gridDatumNcVar.putVar( ( *iter )->GetData( ) );
            }
        } catch( netCDF::exceptions::NcException& e ) {
            e.what( );
            return false;
        }
    }
    return true;
}

bool FileWriter::WriteGroupDatums( Types::GroupDatumVector& groupDatumVector, const unsigned temporalResolution ) const {
    if( groupDatumVector.size( ) > 0 ) {
        std::string filePath;
        unsigned* timeArray;
        unsigned lengthOfTime;

        if( temporalResolution == Constants::eYear ) {
            filePath = Constants::cAnnualGroupOutputsFileName;
            timeArray = Parameters::Get( )->GetYearArray( );
            lengthOfTime = Parameters::Get( )->GetLengthOfSimulationInYears( );
        } else if( temporalResolution == Constants::eMonth ) {
            filePath = Constants::cMonthlyGroupOutputsFileName;
            timeArray = Parameters::Get( )->GetTimeStepArray( );
            lengthOfTime = Parameters::Get( )->GetLengthOfSimulationInMonths( );
        } else
            return false;

        filePath.insert( 0, mOutputDirectory );
        try {
            netCDF::NcFile groupOutputsNcFile( filePath, netCDF::NcFile::replace ); // Creates file

            netCDF::NcDim functionalGroupDim = groupOutputsNcFile.addDim( Constants::cGroupDatumTypeName, FunctionalGroups::Get( )->GetCohorts( ).GetNumberOfFunctionalGroups( ) );
            netCDF::NcVar functionalGroupNcVar = groupOutputsNcFile.addVar( Constants::cGroupDatumTypeName, netCDF::ncFloat, functionalGroupDim );
            functionalGroupNcVar.putVar( FunctionalGroups::Get( )->GetCohorts( ).GetFunctionalGroupIndicesArray( ) );
            functionalGroupNcVar.putAtt( Constants::cUnitsString, Constants::cFunctionalGroupUnitName );

            netCDF::NcDim timeNcDim = groupOutputsNcFile.addDim( Constants::cTimeVariableNames[ 0 ], lengthOfTime );
            netCDF::NcVar timeNcVar = groupOutputsNcFile.addVar( Constants::cTimeVariableNames[ 0 ], netCDF::ncFloat, timeNcDim );
            timeNcVar.putVar( timeArray );
            timeNcVar.putAtt( Constants::cUnitsString, groupDatumVector[ 0 ]->GetTimeUnit( ) );

            Types::NcDimVector dataDimensions;
            dataDimensions.push_back( functionalGroupDim );
            dataDimensions.push_back( timeNcDim );

            for( Types::GroupDatumVector::iterator iter = groupDatumVector.begin( ); iter != groupDatumVector.end( ); iter++ ) {
                netCDF::NcVar groupDatumNcVar = groupOutputsNcFile.addVar( ( *iter )->GetName( ), netCDF::ncFloat, dataDimensions );
                groupDatumNcVar.putAtt( Constants::cUnitsString, ( *iter )->GetDataUnit( ) );
                groupDatumNcVar.putVar( ( *iter )->GetData( ) );
            }

        } catch( netCDF::exceptions::NcException& e ) {
            e.what( );
            return false;
        }
    }
    return true;
}
