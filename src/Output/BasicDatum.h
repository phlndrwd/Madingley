#ifndef BASICDATUM
#define BASICDATUM

#include "Types.h"
#include "Datum.h"

class BasicDatum: public Datum {
public:
    BasicDatum( const std::string&, const std::string&, const std::string& );

    void Initialise( );

    float GetDatum( );
    
    void SetDatum( const float& );
    void AddToDatum( const float& );

private:
};

#endif

