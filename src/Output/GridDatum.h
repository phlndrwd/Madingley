#ifndef GRIDDATUM
#define GRIDDATUM

#include "Types.h"
#include "Datum.h"

class GridDatum: public Datum {
public:
    GridDatum( const std::string&, const std::string&, const std::string& );

    void Initialise( );

    float GetDatum( const unsigned& );
    
    void SetDatum( const unsigned&, const float& );
    void AddToDatum( const unsigned&, const float& );

private:
};

#endif

