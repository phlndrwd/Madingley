#ifndef FILEWRITER
#define FILEWRITER

#include "Types.h"
#include "Grid.h"
#include "Madingley.h"

class FileWriter {
public:
    FileWriter( );
    ~FileWriter( );

    void WriteFiles( Madingley& );
    bool WriteInputFiles( ) const;

    std::string& GetOutputDirectory( );

private:
    bool InitialiseOutputDirectory( );
    bool WriteBasicOutputs( ) const;
    bool WriteGridOutputs( ) const;
    bool WriteGroupOutputs( ) const;
    bool WriteStateFile( Madingley& ) const;

    bool WriteBasicDatums( Types::BasicDatumVector&, const unsigned ) const;
    bool WriteGridDatums( Types::GridDatumVector&, const unsigned ) const;
    bool WriteGroupDatums( Types::GroupDatumVector&, const unsigned ) const;

    std::string mTimeAndDateString;
    std::string mOutputDirectory;
};

#endif

