#ifndef CATCHSUM
#define	CATCHSUM

#include "Types.h"


class CatchSum {
public:
    CatchSum( const unsigned short& );
    ~CatchSum( );

    unsigned short GetFunctionalGroupID( ) const;
    double GetTotal( ) const;
    double GetCatch( const unsigned& ) const;
    
    void AddToCatch( const unsigned&, const double& );

private:
    
    const unsigned char mFunctionalGroupID;
    
    Types::CatchSums mCatchSums;
};

#endif
