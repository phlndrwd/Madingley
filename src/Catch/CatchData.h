#ifndef CATCHDATA
#define CATCHDATA

#include "Types.h"

class CatchData {
public:
    ~CatchData( );
    static Types::CatchDataPointer Get( );

    void Initialise( );
    void InitialiseFunctionalGroupLookup( const Types::StringMatrix& );
    
    unsigned GetFunctionalGroupIndex( const unsigned ) const;

    Types::CatchCellPointer GetCatchCell( const unsigned&, const unsigned& ) const;

    double GetCatchTypeSumForYearCellAndGroup( const unsigned short, const unsigned&, const unsigned char, const unsigned char ) const;
    double GetTotalCatchForYearCellAndGroup( const unsigned short, const unsigned&, const unsigned char ) const;
    
    void SetCatchTypeSumOnYearCellAndGroup( const unsigned short, const unsigned&, const unsigned char, const unsigned char, const double& );

private:
    CatchData( );
    static Types::CatchDataPointer mThis;

    Types::CatchCellMatrix mCatchCells;
    Types::UnsignedVector mFunctionalGroupLookup;
};

#endif