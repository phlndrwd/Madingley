#ifndef CATCHCELL
#define CATCHCELL

#include "Types.h"

class CatchCell {
public:
    CatchCell( Types::CatchSumVector&, const short&, const unsigned& );
    ~CatchCell( );

    short GetYearIndex( ) const;
    unsigned GetCellID( ) const;

    double GetCatchTotalFromGroup( const short& );
    double GetCatchTypeSumFromGroup( const short&, const short& );
    void AddCatchTypeSumToGroup( const short&, const short&, const double& );

private:
    Types::CatchSumVector mCatchSumVector;

    const short mYearIndex;
    const unsigned mCellID;
};

#endif

