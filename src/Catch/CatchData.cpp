#include "CatchData.h"

#include "CatchCell.h"
#include "CatchSum.h"
#include "Parameters.h"
#include "FunctionalGroups.h"
#include "Strings.h"

Types::CatchDataPointer CatchData::mThis = NULL;

Types::CatchDataPointer CatchData::Get( ) {
    if( mThis == NULL ) mThis = new CatchData( );
    return mThis;
}

CatchData::~CatchData( ) {
    for( unsigned char yearIndex = 0; yearIndex < Parameters::Get( )->GetLengthOfSimulationInYears( ); ++yearIndex ) {
        for( unsigned cellIndex = 0; cellIndex < Parameters::Get( )->GetNumberOfGridCells( ); ++cellIndex ) {
            delete mCatchCells[ yearIndex ][ cellIndex ];
        }
        mCatchCells[ yearIndex ].clear( );
    }
    mCatchCells.clear( );

    if( mThis != NULL ) {
        delete mThis;
    }
}

CatchData::CatchData( ) {

}

void CatchData::Initialise( ) {
    unsigned numberOfFunctionalGroups = FunctionalGroups::Get( )->GetCohorts( ).GetNumberOfFunctionalGroups( );
    mCatchCells.resize( Parameters::Get( )->GetLengthOfSimulationInYears( ) );

    for( unsigned yearIndex = 0; yearIndex < Parameters::Get( )->GetLengthOfSimulationInYears( ); ++yearIndex ) {
        mCatchCells[ yearIndex ].resize( Parameters::Get( )->GetNumberOfGridCells( ) );

        for( unsigned cellIndex = 0; cellIndex < Parameters::Get( )->GetNumberOfGridCells( ); ++cellIndex ) {
            Types::CatchSumVector catchSumVector;
            catchSumVector.resize( numberOfFunctionalGroups, 0 );

            for( unsigned functionalGroupIndex = 0; functionalGroupIndex < numberOfFunctionalGroups; ++functionalGroupIndex ) {
                catchSumVector[ functionalGroupIndex ] = new CatchSum( functionalGroupIndex );
            }
            mCatchCells[ yearIndex ][ cellIndex ] = new CatchCell( catchSumVector, yearIndex, cellIndex );
        }
    }
}

void CatchData::InitialiseFunctionalGroupLookup( const Types::StringMatrix& functionalGroupLookupData ) {
    unsigned numberOfDataFunctionalGroups = functionalGroupLookupData.size( );
    mFunctionalGroupLookup.resize( numberOfDataFunctionalGroups );
    for( unsigned dataIndex = 0; dataIndex < numberOfDataFunctionalGroups; dataIndex++ )
        mFunctionalGroupLookup[ dataIndex ] = Strings::Get( )->StringToNumber( functionalGroupLookupData[ dataIndex ][ Constants::eMadingleyIndex ] );
}

unsigned CatchData::GetFunctionalGroupIndex( const unsigned index ) const {
    return mFunctionalGroupLookup[ index ];
}

Types::CatchCellPointer CatchData::GetCatchCell( const unsigned& yearIndex, const unsigned& cellIndex ) const {
    return mCatchCells[ yearIndex ][ cellIndex ];
}

void CatchData::SetCatchTypeSumOnYearCellAndGroup( const unsigned short yearIndex, const unsigned& cellIndex, const unsigned char functionalGroupIndex, const unsigned char catchTypeID, const double& sumCatch ) {
    mCatchCells[ yearIndex ][ cellIndex ]->AddCatchTypeSumToGroup( functionalGroupIndex, catchTypeID, sumCatch );
}

double CatchData::GetCatchTypeSumForYearCellAndGroup( const unsigned short yearIndex, const unsigned& cellIndex, const unsigned char functionalGroupIndex, const unsigned char catchTypeID ) const {
    return mCatchCells[ yearIndex ][ cellIndex ]->GetCatchTypeSumFromGroup( functionalGroupIndex, catchTypeID );
}

double CatchData::GetTotalCatchForYearCellAndGroup( const unsigned short yearIndex, const unsigned& cellIndex, const unsigned char functionalGroupIndex ) const {
    return mCatchCells[ yearIndex ][ cellIndex ]->GetCatchTotalFromGroup( functionalGroupIndex );
}
