#include "CatchCell.h"
#include "CatchSum.h"

CatchCell::CatchCell( Types::CatchSumVector& catchSumVector, const short& yearIndex, const unsigned& cellID ): mYearIndex( yearIndex ), mCellID( cellID ) {
    mCatchSumVector = catchSumVector;
}

CatchCell::~CatchCell( ) {
    for( unsigned char index = 0; index < mCatchSumVector.size( ); ++index ) {
        delete mCatchSumVector[ index ];
    }
    mCatchSumVector.clear( );
}

short CatchCell::GetYearIndex( ) const {
    return mYearIndex;
}

unsigned CatchCell::GetCellID( ) const {
    return mCellID;
}

double CatchCell::GetCatchTotalFromGroup( const short& functionalGroupIndex ) {
    return mCatchSumVector[ functionalGroupIndex ]->GetTotal( );
}

double CatchCell::GetCatchTypeSumFromGroup( const short& functionalGroupIndex, const short& catchTypeID ) {
    return mCatchSumVector[ functionalGroupIndex ]->GetCatch( catchTypeID );
}

void CatchCell::AddCatchTypeSumToGroup( const short& functionalGroupIndex, const short& catchTypeID, const double& catchSum ) {
    mCatchSumVector[ functionalGroupIndex ]->AddToCatch( catchTypeID, catchSum );
}
