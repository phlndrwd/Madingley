#include "CatchSum.h"
#include "Constants.h"

CatchSum::CatchSum( const unsigned short& functionalGroupIndex ): mFunctionalGroupID( functionalGroupIndex ) {
    mCatchSums[ Constants::eReported ] = 0;
    mCatchSums[ Constants::eDiscards ] = 0;
    mCatchSums[ Constants::eUnreported ] = 0;
}

CatchSum::~CatchSum( ) {

}

unsigned short CatchSum::GetFunctionalGroupID( ) const {
    return mFunctionalGroupID;
}

double CatchSum::GetTotal( ) const {
    return mCatchSums[ Constants::eReported ] + mCatchSums[ Constants::eDiscards ] + mCatchSums[ Constants::eUnreported ];
}

double CatchSum::GetCatch( const unsigned& catchTypeID ) const {
    return mCatchSums[ catchTypeID ];
}

void CatchSum::AddToCatch( const unsigned& catchTypeID, const double& addition ) {
    mCatchSums[ catchTypeID ] += addition;
}
