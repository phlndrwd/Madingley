#ifndef DATE
#define	DATE

#include "Types.h"
#include "Constants.h"

#include <sys/time.h>   // For struct timeval
#include <iomanip>

class Date {
public:
    Date( );
    ~Date( );

    static std::string GetDateAndTimeString( const std::string format = Constants::cOutDirectoryDateFormat );
    static std::tm ParseDateString( const std::string&, const std::string format = Constants::cInputDateFormat );
    static int MonthsBetweenDates( const std::string&, const std::string& );
    static unsigned GetYearOfDate( const std::string& );
};

#endif

