#include "RandomSeed.h"
#include "Parameters.h"

Types::RandomSeedPointer RandomSeed::mThis = NULL;

Types::RandomSeedPointer RandomSeed::Get( ) {
    if( mThis == NULL ) mThis = new RandomSeed( );

    return mThis;
}

RandomSeed::RandomSeed( ) {
    mRandom.Reset( );
    mRandom.SetSeed( Parameters::Get( )->GetRandomSeed( ) );
}

RandomSeed::~RandomSeed( ) {
    if( mThis != NULL ) delete mThis;
}

unsigned RandomSeed::Seed( ) {
    return mRandom.GetUint( );
}
