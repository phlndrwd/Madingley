#include "Date.h"

#include "Constants.h"

Date::Date( ) {

}

Date::~Date( ) {

}

std::string Date::GetDateAndTimeString( const std::string format ) {
    char dateTimeChar[ Constants::cDateTimeBufferSize ];
    timeval timeNow;

    gettimeofday( &timeNow, NULL );
    time_t rawtime = static_cast < time_t >( timeNow.tv_sec );

    struct tm* timeinfo;
    timeinfo = localtime( &rawtime );

    strftime( dateTimeChar, Constants::cDateTimeBufferSize, format.c_str( ), timeinfo );
    std::string dateTime( dateTimeChar );

    return dateTime;
}

std::tm Date::ParseDateString( const std::string& dateString, const std::string format ) {
    std::tm date = { };
    strptime( dateString.c_str( ), format.c_str( ), &date );

    return date;
}

int Date::MonthsBetweenDates( const std::string& startDate, const std::string& endDate ) {

    std::tm startStruct = ParseDateString( startDate );
    std::tm endStruct = ParseDateString( endDate );

    double numberOfMonths = 0;

    numberOfMonths += ( endStruct.tm_year - startStruct.tm_year ) * 12;
    numberOfMonths += endStruct.tm_mon - startStruct.tm_mon;
    numberOfMonths += ( endStruct.tm_mday - startStruct.tm_mday ) / 29.0;

    return floor( numberOfMonths + 0.5 );
}

unsigned Date::GetYearOfDate( const std::string& dateString ) {
    std::tm date = ParseDateString( dateString );

    return date.tm_year + 1900;
}