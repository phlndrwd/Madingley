#ifndef RANDOMSEED
#define	RANDOMSEED

#include "NonStaticSimpleRNG.h"
#include "Types.h"

class RandomSeed {
    public:
    static Types::RandomSeedPointer Get( );

    ~RandomSeed( );

    unsigned Seed( );
    
private:
    RandomSeed( );
    static Types::RandomSeedPointer mThis;
    
    NonStaticSimpleRNG mRandom;
};

#endif

