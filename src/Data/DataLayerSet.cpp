#include "DataLayerSet.h"

#include "DataLayer2D.h"
#include "DataLayer2DwithTime.h"
#include "Constants.h"
#include "Strings.h"
#include "Parameters.h"

Types::DataLayerSetPointer DataLayerSet::mThis = NULL;

Types::DataLayerSetPointer DataLayerSet::Get( ) {
    if( mThis == NULL )
        mThis = new DataLayerSet( );

    return mThis;
}

DataLayerSet::DataLayerSet( ) {

}

DataLayerSet::~DataLayerSet( ) {
    for( Types::DataLayerMap::iterator iter = mDataLayerMap.begin( ); iter != mDataLayerMap.end( ); iter++ ) {
        delete iter->second;
    }
}

Types::DataLayerPointer DataLayerSet::CreateDataLayerWithName( const std::string& name, const bool isMonthly ) {
    Types::DataLayerMap::iterator iter = mDataLayerMap.find( name );
    Types::DataLayerPointer dataLayer = NULL;

    if( iter != mDataLayerMap.end( ) )
        dataLayer = iter->second;
    else {
        if( isMonthly == true )
            dataLayer = new DataLayer2DwithTime( name, Parameters::Get( )->GetSizeOfMonthlyGridDatum( ) );
        else
            dataLayer = new DataLayer2D( name, Parameters::Get( )->GetNumberOfGridCells( ) );
        mDataLayerMap.insert( std::pair< std::string, Types::DataLayerPointer >( name, dataLayer ) );
    }

    return dataLayer;
}

bool DataLayerSet::VerifyDataLayers( ) {
    bool returnValue = true;
    for( Types::DataLayerMap::iterator iter = mDataLayerMap.begin( ); iter != mDataLayerMap.end( ); iter++ ) {
        if( iter->second->Verify( ) == false ) {
            std::cout << "ERROR> Data layer verification failed for \"" << iter->first << "\"." << std::endl;
            returnValue = false;
        }
    }
    return returnValue;
}

Types::DataLayerPointer DataLayerSet::GetDataLayerWithName( const std::string & name ) {
    Types::DataLayerPointer dataLayer = NULL;
    Types::DataLayerMap::iterator iter = mDataLayerMap.find( name );

    if( iter != mDataLayerMap.end( ) )
        dataLayer = iter->second;
    else
        std::cout << "ERROR> DataLayer with name \"" << name << "\" was not found." << std::endl;

    return dataLayer;
}

float DataLayerSet::GetDataAtCellIndexFor( const std::string& name, const unsigned cellIndex ) {
    float value = Constants::cMissingValue;
    Types::DataLayerPointer dataLayer = GetDataLayerWithName( name );

    if( dataLayer != NULL ) value = dataLayer->GetDataAtCellIndex( cellIndex );

    return value;
}

float DataLayerSet::GetDataAtIndexFor( const std::string& name, const unsigned index ) {
    float value = Constants::cMissingValue;
    Types::DataLayerPointer dataLayer = GetDataLayerWithName( name );

    if( dataLayer != NULL ) value = dataLayer->GetDataAtIndex( index );

    return value;
}
