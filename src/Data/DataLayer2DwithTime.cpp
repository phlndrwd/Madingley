#include "DataLayer2DwithTime.h"
#include "Constants.h"
#include "Timing.h"
#include "Parameters.h"

DataLayer2DwithTime::DataLayer2DwithTime( const std::string& name, const unsigned size ) : DataLayer::DataLayer( name, size ) {

}

DataLayer2DwithTime::~DataLayer2DwithTime( ) {

}

float DataLayer2DwithTime::GetDataAtCellIndex( const unsigned cellIndex ) {
    float data = Constants::cMissingValue;
    unsigned dataIndex = cellIndex + ( Timing::Get( )->GetTimeStep( ) * Parameters::Get( )->GetNumberOfGridCells( ) );
    if( dataIndex < mSize ) {
        data = mData[ dataIndex ];
    } else
        std::cout << "ERROR> Index of " << dataIndex << " exceeds size (" << mSize << ") of variable \"" << mName << "\"." << std::endl;
    return data;
}

bool DataLayer2DwithTime::Verify( ) const {
    int indexDiff = Parameters::Get( )->GetSizeOfMonthlyGridDatum( ) - mUserIndex - 1;
    return indexDiff == 0;
}