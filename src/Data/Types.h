#ifndef TYPES
#define TYPES

#include <vector>       // For std::vector
#include <sstream>      // For std::stringstream
#include <fstream>      // For std::ofstream
#include <algorithm>    // For std::sort
#include <assert.h>     // For assert
#include <map>          // For map
#include <sys/stat.h>   // For mkdir
#include <netcdf>       // For netCDF::NcDim vector
#include <math.h>

class BasicDatum;
class CatchCell;
class CatchData;
class CatchSum;
class Cohort;
class DataCoords;
class DataIndices;
class DataLayer;
class DataLayerSet;
class DataRecorder;
class DispersalSet;
class Environment;
class FileReader;
class FileWriter;
class FunctionalGroups;
class GridCell;
class GridDatum;
class GroupDatum;
class Dispersal;
class InputData;
class InputDatum;
class Layer;
class Initialisation;
class Parameters;
class Processor;
class RandomSeed;
class Stock;
class Strings;
class Timing;
class Variable;

namespace Types {
    // Pointers
    typedef BasicDatum* BasicDatumPointer;
    typedef CatchCell* CatchCellPointer;
    typedef CatchData* CatchDataPointer;
    typedef CatchSum* CatchSumPointer;
    typedef Cohort* CohortPointer;
    typedef DataCoords* DataCoordsPointer;
    typedef DataIndices* DataIndicesPointer;
    typedef DataLayer* DataLayerPointer;
    typedef DataLayerSet* DataLayerSetPointer;
    typedef DataRecorder* DataRecorderPointer;
    typedef DispersalSet* DispersalSetPointer;
    typedef Environment* EnvironmentPointer;
    typedef FileReader* FileReaderPointer;
    typedef FileWriter* FileWriterPointer;
    typedef FunctionalGroups* FunctionalGroupsPointer;
    typedef GridDatum* GridDatumPointer;
    typedef GridCell* GridCellPointer;
    typedef GroupDatum* GroupDatumPointer;
    typedef Dispersal* DispersalPointer;
    typedef InputData* InputDataPointer;
    typedef InputDatum* InputDatumPointer;
    typedef Layer* LayerPointer;
    typedef Parameters* ParametersPointer;
    typedef Processor* ProcessorPointer;
    typedef RandomSeed* RandomSeedPointer;
    typedef Strings* StringsPointer;
    typedef Timing* TimeStepPointer;
    typedef Variable* VariablePointer;

    // Containers of pointers/objects
    typedef std::map< long, GridCell > GridCellMap;

    typedef std::map< std::string, BasicDatumPointer > BasicDatumMap;
    typedef std::map< std::string, DataLayerPointer > DataLayerMap;
    typedef std::map< std::string, DispersalPointer > DispersalMap;
    typedef std::map< std::string, GridDatumPointer > GridDatumMap;
    typedef std::map< std::string, GroupDatumPointer > GroupDatumMap;
    typedef std::map< std::string, LayerPointer > LayerMap;

    typedef std::pair< DataCoordsPointer, DataIndicesPointer > CoordsIndicesPair;

    typedef std::vector< BasicDatumPointer > BasicDatumVector;
    typedef std::vector< CatchCellPointer > CatchCellVector;
    typedef std::vector< CatchSumPointer > CatchSumVector;
    typedef std::vector< Cohort > CohortVector;
    typedef std::vector< GridDatumPointer > GridDatumVector;
    typedef std::vector< GroupDatumPointer > GroupDatumVector;
    typedef std::vector< InputDatumPointer > InputDatumVector;
    typedef std::vector< netCDF::NcDim > NcDimVector;
    typedef std::vector< Stock > StockVector;
    typedef std::vector< VariablePointer > VariableVector;

    // Containers of primitives
    typedef std::array< double, 3 > CatchSums;
    
    typedef std::map< std::string, double > DoubleMap;
    typedef std::map< std::string, std::string > StringMap;
    typedef std::map< std::string, unsigned > UnsignedMap;

    typedef std::vector< bool > BoolVector;
    typedef std::vector< char > CharVector;
    typedef std::vector< unsigned char > UnsignedCharVector;
    typedef std::vector< double > DoubleVector;
    typedef std::vector< float > FloatVector;
    typedef std::vector< int > IntegerVector;
    typedef std::vector< long > LongVector;
    typedef std::vector< long double > LongDoubleVector;
    typedef std::vector< short > ShortVector;
    typedef std::vector< unsigned > UnsignedVector;
    typedef std::vector< unsigned long long > UnsignedLongLongVector;
    typedef std::vector< unsigned short > UnsignedShortVector;
    typedef std::vector< std::string > StringVector;

    // Containers of containers of pointers/objects
    typedef std::map< int, StockVector > StocksMap;

    typedef std::vector< CatchCellVector > CatchCellMatrix;
    typedef std::vector< CoordsIndicesPair > CoordsIndicesVector;
    typedef std::vector< CohortVector > Cohort2DVector;
    typedef std::vector< DoubleVector > DoubleMatrix;
    typedef std::vector< StringVector > StringMatrix;
    typedef std::vector< UnsignedVector > UnsignedMatrix;

    // Containers of containers of primitives
    typedef std::map< std::string, DoubleMap > Double2DMap;
    typedef std::map< std::string, DoubleVector > DoubleVectorMap;
    typedef std::map< std::string, IntegerVector > IntegerVectorMap;
    typedef std::map< std::string, StringVector > StringVectorMap;

    // Container of containers of containers of primitives
    typedef std::map< std::string, IntegerVectorMap > Integer2DVectorMap;

    typedef std::vector< DoubleMatrix > Double3DMatrix;
}

#endif
