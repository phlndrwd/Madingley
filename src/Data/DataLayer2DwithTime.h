#ifndef DATALAYER2DWITHTIME
#define DATALAYER2DWITHTIME

#include "DataLayer.h"

class DataLayer2DwithTime : public DataLayer {
public:
    DataLayer2DwithTime( const std::string&, const unsigned );
    ~DataLayer2DwithTime( );

    float GetDataAtCellIndex( const unsigned );
    bool Verify( ) const;

private:
};

#endif
