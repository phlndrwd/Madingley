#ifndef DATALAYER2D
#define DATALAYER2D

#include "DataLayer.h"

class DataLayer2D : public DataLayer {
public:
    DataLayer2D( const std::string&, const unsigned );
    ~DataLayer2D( );

    float GetDataAtCellIndex( const unsigned );
    bool Verify( ) const;
private:

};

#endif

