#include "DataLayer.h"
#include "Parameters.h"
#include "Timing.h"

DataLayer::DataLayer( const std::string& name, const unsigned size ) {
    mName = name;
    mSize = size;
    mData = new float[ mSize ];
    mUserIndex = 0;
}

float DataLayer::GetDataAtIndex( const unsigned& index ) const {
    float data = Constants::cMissingValue;
    if( index < mSize )
        data = mData[ index ];
    else
        std::cout << "ERROR> Index of " << index << " exceeds size (" << mSize << ") of variable \"" << mName << "\"." << std::endl;

    return data;
}

void DataLayer::SetDataAtIndex( const unsigned& index, const float data ) {
    if( index < mSize ) {
        mData[ index ] = data;
        mUserIndex = index;
    } else
        std::cout << "ERROR> Index of " << index << " exceeds size (" << mSize << ") of variable \"" << mName << "\"." << std::endl;
}

unsigned DataLayer::GetUserIndex( ) {
    if( mUserIndex != 0 )
        mUserIndex += 1;
    return mUserIndex;
}
