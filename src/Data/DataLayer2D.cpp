#include "DataLayer2D.h"
#include "Constants.h"
#include "Parameters.h"

DataLayer2D::DataLayer2D( const std::string& name, const unsigned size ) : DataLayer::DataLayer( name, size ) {

}

DataLayer2D::~DataLayer2D( ) {

}

float DataLayer2D::GetDataAtCellIndex( const unsigned cellIndex ) {
    float data = Constants::cMissingValue;
    if( cellIndex < mSize ) {
        data = mData[ cellIndex ];
    } else
        std::cout << "ERROR> Index of " << cellIndex << " exceeds size (" << mSize << ") of variable \"" << mName << "\"." << std::endl;

    return data;
}

bool DataLayer2D::Verify( ) const {
    int indexDiff = Parameters::Get( )->GetNumberOfGridCells( ) - mUserIndex - 1;
    return indexDiff == 0;
}