#ifndef DATALAYERSET
#define DATALAYERSET

#include "Types.h"

class DataLayerSet {
public:
    ~DataLayerSet( );
    static Types::DataLayerSetPointer Get( );

    Types::DataLayerPointer CreateDataLayerWithName( const std::string&, const bool isMonthly = true );
    bool VerifyDataLayers( );
    
    Types::DataLayerPointer GetDataLayerWithName( const std::string& );
    
    float GetDataAtCellIndexFor( const std::string&, const unsigned );
    float GetDataAtIndexFor( const std::string&, const unsigned );
private:
    DataLayerSet( );

    static Types::DataLayerSetPointer mThis;

    Types::DataLayerMap mDataLayerMap;
    Types::UnsignedMap mMaxTimeIndexSetForEachDatum;

};

#endif

