#ifndef DATALAYER
#define DATALAYER

#include "Types.h"
#include "Constants.h"

class DataLayer {
public:
    DataLayer( const std::string&, const unsigned );

    virtual ~DataLayer( ) {
        delete[ ] mData;
    };

    virtual float GetDataAtCellIndex( const unsigned ) = 0;
    virtual bool Verify( ) const = 0;

    float GetDataAtIndex( const unsigned& ) const;
    void SetDataAtIndex( const unsigned&, const float );

    unsigned GetUserIndex( );

protected:
    std::string mName;
    unsigned mSize;
    unsigned mUserIndex;
    float* mData;
};

#endif

