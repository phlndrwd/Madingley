#ifndef PARAMETERS
#define PARAMETERS

#include "Types.h"

class Parameters {
public:
    ~Parameters( );
    static Types::ParametersPointer Get( );

    bool Initialise( const Types::StringMatrix& );

    // User defined parameters
    std::string GetExperimentName( ) const;
    std::string GetRootDataDirectory( ) const;
    std::string GetStartDateString( ) const;
    std::string GetEndDataString( ) const;
    unsigned GetSpinUpYears( ) const;
    bool GetApplyCatchData( ) const;
    float GetCatchCoefficient( ) const;
    int GetUserMinimumLongitude( ) const;
    int GetUserMaximumLongitude( ) const;
    int GetUserMinimumLatitude( ) const;
    int GetUserMaximumLatitude( ) const;
    float GetGridCellSize( ) const;
    float GetExtinctionThreshold( ) const;
    unsigned GetMaximumNumberOfCohortsPerCell( ) const;
    float GetPlanktonSizeThreshold( ) const;
    bool GetReadModelState( ) const;
    bool GetWriteModelState( ) const;
    unsigned GetRandomSeed( ) const;

    void SetExperimentName( const std::string& );
    void SetRootDataDirectory( const std::string& );
    void SetStartDateString( const std::string& );
    void SetEndDateString( const std::string& );
    void SetSpinUpYears( const unsigned& );
    void SetApplyCatchData( const bool );
    void SetCatchCoefficient( const float& );
    void SetUserMinimumLongitude( const int& );
    void SetUserMaximumLongitude( const int& );
    void SetUserMinimumLatitude( const int& );
    void SetUserMaximumLatitude( const int& );
    void SetGridCellSize( const float& );
    void SetExtinctionThreshold( const float& );
    void SetMaximumNumberOfCohortsPerCell( const unsigned& );
    void SetPlanktonSizeThreshold( const float& );
    void SetReadModelState( const bool );
    void SetWriteModelState( const bool );
    void SetRandomSeed( const unsigned& );

    // Calculated parameters
    std::string& GetDataBasePath( );
    unsigned GetTemporalConfiguration( ) const;
    unsigned GetLengthOfSimulationInMonths( ) const;
    unsigned GetLengthOfSimulationInYears( ) const;
    unsigned GetNumberOfGridCells( ) const;
    unsigned GetLengthDataLongitudeArray( ) const;
    unsigned GetLengthDataLatitudeArray( ) const;
    unsigned GetDataIndexOfUserMinimumLongitude( ) const;
    unsigned GetDataIndexOfUserMaximumLongitude( ) const;
    unsigned GetDataIndexOfUserMinimumLatitude( ) const;
    unsigned GetDataIndexOfUserMaximumLatitude( ) const;
    unsigned GetLengthUserLongitudeArray( ) const;
    unsigned GetLengthUserLatitudeArray( ) const;

    unsigned GetSizeOfAnnualGridDatum( ) const;
    unsigned GetSizeOfMonthlyGridDatum( ) const;

    float GetDataLongitudeAtIndex( const unsigned& ) const;
    float GetDataLatitudeAtIndex( const unsigned& ) const;
    float GetUserLongitudeAtIndex( const unsigned& ) const;
    float GetUserLatitudeAtIndex( const unsigned& ) const;

    unsigned* GetTimeStepArray( ) const;
    unsigned* GetYearArray( ) const;
    float* GetUserLongitudeArray( ) const;
    float* GetUserLatitudeArray( ) const;

    int GetCellIndexFromDataIndices( const unsigned&, const unsigned& ) const;
    Types::DataCoordsPointer GetDataCoordsFromCellIndex( const unsigned& ) const;
    Types::DataIndicesPointer GetDataIndicesFromCellIndex( const unsigned& ) const;

private:
    Parameters( );
    bool CalculateParameters( );

    static Types::ParametersPointer mThis;

    // User defined parameters
    std::string mExperimentName;
    std::string mRootDataDirectory;
    std::string mStartDateString;
    std::string mEndDateString;
    unsigned mSpinUpYears;
    bool mApplyCatchData;
    float mCatchCoefficient;
    int mUserMinimumLongitude;
    int mUserMaximumLongitude;
    int mUserMinimumLatitude;
    int mUserMaximumLatitude;
    float mGridCellSize;
    float mExtinctionThreshold;
    unsigned mMaximumNumberOfCohortsPerCell;
    float mPlanktonSizeThreshold;
    bool mReadModelState;
    bool mWriteModelState;
    unsigned mRandomSeed;

    // Calculated parameters
    std::string mDataBasePath;
    unsigned mTemporalConfiguration;
    unsigned mLengthOfSimulationInMonths;
    unsigned mLengthOfSimulationInYears;
    unsigned mNumberOfGridCells;
    unsigned mLengthDataLongitudeArray;
    unsigned mLengthDataLatitudeArray;
    unsigned mLengthUserLongitudeArray;
    unsigned mLengthUserLatitudeArray;
    unsigned mDataIndexOfUserMinimumLongitude;
    unsigned mDataIndexOfUserMaximumLongitude;
    unsigned mDataIndexOfUserMinimumLatitude;
    unsigned mDataIndexOfUserMaximumLatitude;
    unsigned mSizeOfMonthlyGridDatum;
    unsigned mSizeOfAnnualGridDatum;
    unsigned* mTimeStepArray;
    unsigned* mYearArray;
    float* mDataLongitudeArray;
    float* mDataLatitudeArray;
    float* mUserLongitudeArray;
    float* mUserLatitudeArray;

    Types::CoordsIndicesVector mCoordsIndicesLookup;
};

#endif

