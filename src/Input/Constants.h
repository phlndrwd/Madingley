#ifndef CONSTANTS
#define CONSTANTS

#include <string>

namespace Constants {

    ///////////// STUFF THAT CAN BE DELETED FOLLOWING INTEGRATION //////////////

    enum eVariableTypes {
        eLongitude,
        eLatitude,
        eTime,
        eDepth,
        eOther
    };
    
    const std::string cTimeStepUnits = "month";

    ///////////// STUFF THAT CAN BE DELETED FOLLOWING INTEGRATION //////////////

    enum eTimeResolution {
        eYear,
        eMonth,
        eNoTime
    };

    enum eTemporalConfiguration {
        eUndefined,
        eHistoricalOnly,
        eFutureOnly,
        eHistoricalAndFuture
    };

    enum eInputDataClass {
        eUnclassified,
        eHistorical,
        eFuture
    };

    enum eLandMaskDefinition {
        eLand,
        eOcean
    };

    enum eParametersMetadata {
        eParameterName,
        eParameterValue
    };

    enum eOutputControlParametersMetadata {
        eDatumName,
        eDatumType,
        eTimeUnit,
        eDataUnit
    };

    enum eInputMetadata {
        eInternalName,
        eDataFilePath,
        eDefaultVariable
    };

    enum eCatchDataTables {
        eCellIDTable,
        eCatchDataTable
    };
    
    enum eFunctionalGroupLookupTable {
        eDataIndex,
        eMadingleyIndex,
        eDescription
    };

    enum eCatchMetadata {
        eCatchTableName,
        eCatchFilePath
    };

    enum eCellIDTableColumns {
        eCellCellID,
        eCellLongitude,
        eCellLatitude
    };

    enum eCatchDataTableColumns {
        eCatchYear,
        eCatchCellID,
        eFunctionalGroupIndex,
        eCatchTypeID,
        eSumCatch
    };

    enum eCatchType {
        eReported,
        eDiscards,
        eUnreported
    };

    enum eStateFileLines {
        eMaximumCohortsPerCell,
        eLengthLongitude,
        eLengthLatitude,
        eFirstGridCell
    };

    // Input and output
    const std::string cLongitudeVariableNames[ ] = { "lon", "long", "longitude", "x" };
    const std::string cLatitudeVariableNames[ ] = { "lat", "lats", "latitude", "y" };
    const std::string cDepthVariableNames[ ] = { "dep", "depth", "m", "z" };
    const std::string cTimeVariableNames[ ] = { "time", "time step", "month", "t" };
    const char cDataDelimiterValue = ',';
    const char cCommentCharacter = '#';
    const char cFolderDelimiter = '/';
    const char cWhiteSpaceCharacter = ' ';
    // Input directories
    const std::string cConfigurationDirectory = "./input/";
    // Input files
    const std::string cInputParametersFileName = "InputParameters.csv";
    const std::string cInputMetadataFileName = "InputMetadata.csv";
    const std::string cOutputVariablesFileName = "OutputVariables.csv";
    const std::string cCatchMetadataFileName = "CatchMetadata.csv";
    const std::string cCohortDefinitionsFileName = "CohortDefinitions.csv";
    const std::string cStockDefinitionsFileName = "StockDefinitions.csv";
    const std::string cFunctionalGroupLookupFileName = "FunctionalGroupLookup.csv";
    // Input parameters
    const std::string cInputDateFormat = "%d-%m-%Y";
    // Output directories
    const std::string cOutputBaseDirectory = "./output/";
    const std::string cOutDirectoryDateFormat = "%Y-%m-%d_%H-%M-%S";
    const std::string cTagSubdirectoryName = "Tag_";
    const std::string cFunctionalGroupSubdirectoryName = "FunctionalGroup_";
    const int cOutputFolderPermissions = 0775;
    // Output files
    const std::string cAnnualBasicOutputsFileName = "AnnualBasicOutputs.nc";
    const std::string cMonthlyBasicOutputsFileName = "MonthlyBasicOutputs.nc";
    const std::string cAnnualGridOutputsFileName = "AnnualGridOutputs.nc";
    const std::string cMonthlyGridOutputsFileName = "MonthlyGridOutputs.nc";
    const std::string cNoTimeGridOutputsFileName = "NoTimeGridOutputs.nc";
    const std::string cAnnualGroupOutputsFileName = "AnnualGroupOutputs.nc";
    const std::string cMonthlyGroupOutputsFileName = "MonthlyGroupOutputs.nc";
    const std::string cCohortEventsFileName = "CohortEvents.csv";
    const std::string cLocationDataFileName = "Location.csv";
    const std::string cFileNameExtension = ".csv";
    const std::string cStateFileName = "State.csv";
    // Output parameters
    const std::string cMonthsOfTheYear[ ] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    const std::string cMonthTimeUnitName = "month";
    const std::string cYearTimeUnitName = "year";
    const std::string cNoTimeUnitName = "none";
    const std::string cLongitudeUnitName = "degrees east";
    const std::string cLatitudeUnitName = "degrees north";
    const std::string cFunctionalGroupUnitName = "index";
    const std::string cUnitsString = "units";
    const std::string cCompleteDateFormat = "%c";
    const std::string cBasicDatumTypeName = "basic";
    const std::string cGridDatumTypeName = "grid";
    const std::string cGroupDatumTypeName = "group";
    const int cDateTimeBufferSize = 25;
    const int cMissingValue = -9999;
    // State file characters
    const std::string cGridCellString = "G";
    const std::string cStockString = "S";
    const std::string cCohortString = "C";
    // Configuration parameters
    const std::string cDataHistoricalStartDate = "01-01-1950";
    const std::string cDataHistoricalEndDate = "31-12-2005";
    const std::string cDataFutureStartDate = "01-01-2006";
    const std::string cDataFutureEndDate = "31-12-2100";
}

#endif
