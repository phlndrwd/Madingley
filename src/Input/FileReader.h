#ifndef FILEREADER
#define FILEREADER

#include "Types.h"
#include "Constants.h"

class FileReader {
public:
    FileReader( );
    ~FileReader( );

    void ReadFiles( );
    void ReadData( std::string& );

    static Types::StringMatrix mMetadata;
private:
    bool ReadTextFile( const std::string&, const bool copyToOutput = true );
    bool ReadCatchDataFile( const std::string&, const Constants::eCatchDataTables );

    bool ReadInputParameters( );
    bool SetUpOutputVariables( );
    bool ReadNetCDFFiles( );
    unsigned ClassifyNetCDFFile( const std::string& );
    bool DefineFunctionalGroups( );
    bool ReadCatchData( );
    bool ReadStateFile( std::string& );

    void ClearMetadata( );

    std::string mFilePath;
    Types::StringVector mMetadataHeadings;
    Types::IntegerVector mAllocatedCellIndices;
};

#endif

