#include "FileReader.h"

#include "CatchData.h"
#include "Strings.h"
#include "Constants.h"
#include "DataLayerSet.h"
#include "DataRecorder.h"
#include "Parameters.h"
#include "DataCoords.h"
#include "FunctionalGroups.h"
#include "Date.h"
#include "DataLayer.h"
#include "Processor.h"
#include "Timing.h"

#include <netcdf>

Types::StringMatrix FileReader::mMetadata;

FileReader::FileReader( ) {

}

FileReader::~FileReader( ) {

}

void FileReader::ReadFiles( ) {
    bool success = false;

    if( DefineFunctionalGroups( ) == true )
        if( ReadInputParameters( ) == true )
            success = SetUpOutputVariables( );

    if( success )
        std::cout << "Files read successfully..." << std::endl;
    else {
        std::cout << "ERROR> File reading failed. System exiting..." << std::endl;
        exit( 1 );
    }
}

void FileReader::ReadData( std::string& stateFile ) {
    bool success = false;

    if( ReadNetCDFFiles( ) == true )
        if( ReadCatchData( ) == true )
            // Reading state file must be called last. The metadata 
            // object is not cleared and may be accessed from a static 
            // context by GridRestart.
            success = ReadStateFile( stateFile );

    if( success == true )
        std::cout << "Data read successfully..." << std::endl;
    else {
        std::cout << "ERROR> Data reading failed. System exiting..." << std::endl;
        exit( 1 );
    }
}

bool FileReader::ReadTextFile( const std::string& filePath, const bool copyToOutput ) {
    ClearMetadata( );
    std::cout << "Reading text file \"" << filePath << "\"..." << std::endl;
    std::ifstream fileStream( filePath.c_str( ), std::ios::in );

    if( fileStream.is_open( ) ) {
        std::string readLine;
        unsigned lineCount = 0;

        while( std::getline( fileStream, readLine ) ) {
            if( readLine.length( ) > 0 ) {
                if( readLine[ 0 ] != Constants::cCommentCharacter ) {
                    readLine = Strings::Get( )->TruncateStringBeforeFirstCharacter( readLine, Constants::cCommentCharacter );
                    if( lineCount > 0 )
                        mMetadata.push_back( Strings::Get( )->StringToWords( readLine, Constants::cDataDelimiterValue ) );
                    else if( lineCount == 0 )
                        mMetadataHeadings = Strings::Get( )->StringToWords( readLine, Constants::cDataDelimiterValue );

                    lineCount++;
                }
            }
        }
        fileStream.close( );

        if( copyToOutput == true ) DataRecorder::Get( )->AddInputFilePath( filePath );
    } else {
        return false;
    }

    return true;
}

bool FileReader::ReadCatchDataFile( const std::string& filePath, const Constants::eCatchDataTables table ) {
    bool success = false;
    std::ifstream fileStream( filePath.c_str( ), std::ios::in );

    if( fileStream.is_open( ) ) {
        std::string readLine;
        unsigned lineCount = 0;

        if( table == Constants::eCellIDTable ) {
            std::cout << "Processing cell ID table..." << std::endl;
            float offset = Parameters::Get( )->GetGridCellSize( ) / 2.0;
            float userLongitudeMinimum = Parameters::Get( )->GetUserLongitudeAtIndex( 0 ) - offset;
            float userLatitudeMinimum = Parameters::Get( )->GetUserLatitudeAtIndex( 0 ) - offset;
            float userLongitudeMaximum = Parameters::Get( )->GetUserLongitudeAtIndex( Parameters::Get( )->GetLengthUserLongitudeArray( ) - 1 ) + offset;
            float userLatitudeMaximum = Parameters::Get( )->GetUserLatitudeAtIndex( Parameters::Get( )->GetLengthUserLatitudeArray( ) - 1 ) + offset;

            while( std::getline( fileStream, readLine ) ) {
                if( lineCount > 0 && readLine[ 0 ] != Constants::cCommentCharacter ) {
                    Types::StringVector readWords = Strings::Get( )->StringToWords( readLine, Constants::cDataDelimiterValue );

                    float dataLongitude = Strings::Get( )->StringToNumber( readWords[ Constants::eCellLongitude ] );
                    float dataLatitude = Strings::Get( )->StringToNumber( readWords[ Constants::eCellLatitude ] );

                    if( dataLongitude < userLongitudeMinimum || dataLatitude < userLatitudeMinimum || dataLongitude > userLongitudeMaximum || dataLatitude > userLatitudeMaximum )
                        mAllocatedCellIndices.push_back( Constants::cMissingValue );
                    else {
                        for( unsigned cellIndex = 0; cellIndex < Parameters::Get( )->GetNumberOfGridCells( ); ++cellIndex ) {
                            Types::DataCoordsPointer coords = Parameters::Get( )->GetDataCoordsFromCellIndex( cellIndex );

                            float minLon = coords->GetLongitude( ) - offset;
                            float maxLon = coords->GetLongitude( ) + offset;
                            float minLat = coords->GetLatitude( ) - offset;
                            float maxLat = coords->GetLatitude( ) + offset;

                            if( dataLongitude > minLon && dataLongitude <= maxLon && dataLatitude > minLat && dataLatitude <= maxLat ) {
                                mAllocatedCellIndices.push_back( cellIndex );
                                break;
                            }
                        }
                    }
                }
                ++lineCount;
            }
        } else if( table == Constants::eCatchDataTable ) {
            std::cout << "Processing catch data table from text file..." << std::endl;
            unsigned firstYearOfData = Timing::Get( )->GetFirstYear( );
            unsigned finalYearOfData = Timing::Get( )->GetLastYear( );
            while( std::getline( fileStream, readLine ) ) {
                if( lineCount > 0 && readLine[ 0 ] != Constants::cCommentCharacter ) {
                    Types::StringVector readWords = Strings::Get( )->StringToWords( readLine, Constants::cDataDelimiterValue );

                    unsigned year = Strings::Get( )->StringToNumber( readWords[ Constants::eCatchYear ] );
                    if( year >= firstYearOfData && year <= finalYearOfData ) {
                        unsigned yearIndex = year - firstYearOfData;
                        unsigned cellID = Strings::Get( )->StringToNumber( readWords[ Constants::eCatchCellID ] ) - 1;
                        int cellIndex = mAllocatedCellIndices[ cellID ];

                        if( cellIndex != Constants::cMissingValue ) {
                            double sumCatch = Strings::Get( )->StringToNumber( readWords[ Constants::eSumCatch ] );
                            sumCatch /= 12.0; // Convert from annual to monthly data
                            sumCatch *= 1000; // Convert from tonnes to kilograms

                            if( sumCatch > 0 ) {
                                unsigned char functionalGroupIndex = CatchData::Get( )->GetFunctionalGroupIndex( Strings::Get( )->StringToNumber( readWords[ Constants::eFunctionalGroupIndex ] ) );
                                unsigned char catchTypeID = Strings::Get( )->StringToNumber( readWords[ Constants::eCatchTypeID ] ) - 1;
                                CatchData::Get( )->SetCatchTypeSumOnYearCellAndGroup( yearIndex, cellIndex, functionalGroupIndex, catchTypeID, sumCatch );
                            }
                        }
                    }
                }
                ++lineCount;
            }
        }
        success = true;
    } else {
        std::cout << "File path \"" << filePath << "\" is invalid." << std::endl;
    }
    fileStream.close( );

    return success;
}

bool FileReader::ReadInputParameters( ) {
    if( ReadTextFile( Constants::cConfigurationDirectory + Constants::cInputParametersFileName ) )
        return( Parameters::Get( )->Initialise( mMetadata ) );

}

bool FileReader::SetUpOutputVariables( ) {
    if( ReadTextFile( Constants::cConfigurationDirectory + Constants::cOutputVariablesFileName ) )
        return DataRecorder::Get( )->Initialise( mMetadata );
}

bool FileReader::ReadNetCDFFiles( ) {
    if( ReadTextFile( Constants::cConfigurationDirectory + Constants::cInputMetadataFileName ) ) {
        unsigned dataTimeSize = 1;
        float* readData = NULL;
        float* timeData = NULL;

        for( unsigned dataFileIndex = 0; dataFileIndex < mMetadata.size( ); dataFileIndex++ ) {
            std::string filePath = Parameters::Get( )->GetDataBasePath( ) + mMetadata[ dataFileIndex ][ Constants::eDataFilePath ];
            std::cout << "Reading NetCDF file \"" << filePath << "\"..." << std::endl;
            try {
                netCDF::NcFile inputNcFile( filePath, netCDF::NcFile::read ); // Open the file for read access
                std::multimap< std::string, netCDF::NcVar > multiMap = inputNcFile.getVars( );
                // Outer variable loop
                for( std::multimap<std::string, netCDF::NcVar>::iterator it = multiMap.begin( ); it != multiMap.end( ); it++ ) {
                    std::string variableName = ( *it ).first;
                    netCDF::NcVar variableNcVar = ( *it ).second;
                    std::vector< netCDF::NcDim > varDims = variableNcVar.getDims( );

                    Types::UnsignedVector variableDimensions;
                    unsigned variableSize = 1;
                    // Inner variable dimension loop
                    for( unsigned dimIndex = 0; dimIndex < varDims.size( ); dimIndex++ ) {
                        variableDimensions.push_back( varDims[ dimIndex ].getSize( ) );
                        variableSize *= varDims[ dimIndex ].getSize( );
                    }
                    if( Strings::Get( )->IsStringInArray( variableName, Constants::cTimeVariableNames ) ) {
                        dataTimeSize = variableSize;
                        timeData = new float[ dataTimeSize ];
                        variableNcVar.getVar( timeData );
                    }
                    if( variableName == Strings::Get( )->ToLowercase( mMetadata[ dataFileIndex ][ Constants::eDefaultVariable ] ) ) {
                        readData = new float[ variableSize ];
                        variableNcVar.getVar( readData );
                    }
                }
                std::string internalName = mMetadata[ dataFileIndex ][ Constants::eInternalName ];
                Types::DataLayerPointer internalDataLayer = DataLayerSet::Get( )->CreateDataLayerWithName( internalName, dataTimeSize > 1 );

                if( internalDataLayer != NULL && internalDataLayer->Verify( ) == false ) {
                    unsigned temporalConfiguration = Parameters::Get( )->GetTemporalConfiguration( );
                    unsigned dataFileClass = ClassifyNetCDFFile( filePath );

                    int startIndex = 0;
                    int endIndex = 1;
                    bool processFile = false;

                    if( ( temporalConfiguration == Constants::eHistoricalOnly || temporalConfiguration == Constants::eHistoricalAndFuture ) && dataFileClass == Constants::eHistorical ) {
                        int startGap = Date::MonthsBetweenDates( Constants::cDataHistoricalStartDate, Parameters::Get( )->GetStartDateString( ) );
                        if( startGap < 0 ) startIndex = 0;
                        else startIndex = startGap;

                        int endGap = Date::MonthsBetweenDates( Parameters::Get( )->GetEndDataString( ), Constants::cDataHistoricalEndDate );
                        if( endGap < 0 ) endIndex = Date::MonthsBetweenDates( Constants::cDataHistoricalStartDate, Constants::cDataHistoricalEndDate );
                        else endIndex = Date::MonthsBetweenDates( Parameters::Get( )->GetStartDateString( ), Parameters::Get( )->GetEndDataString( ) ) + startIndex;

                        processFile = true;
                    } else if( ( temporalConfiguration == Constants::eFutureOnly || temporalConfiguration == Constants::eHistoricalAndFuture ) && dataFileClass == Constants::eFuture ) {
                        int startGap = Date::MonthsBetweenDates( Constants::cDataFutureStartDate, Parameters::Get( )->GetStartDateString( ) );
                        if( startGap < 0 ) startIndex = 0;
                        else startIndex = startGap;

                        int endGap = Date::MonthsBetweenDates( Parameters::Get( )->GetEndDataString( ), Constants::cDataFutureEndDate );
                        if( endGap < 0 ) endIndex = Date::MonthsBetweenDates( Constants::cDataFutureStartDate, Constants::cDataFutureEndDate );
                        else endIndex = Date::MonthsBetweenDates( Constants::cDataFutureStartDate, Parameters::Get( )->GetEndDataString( ) ) + startIndex;

                        processFile = true;
                    } else if( dataFileClass == Constants::eUnclassified )
                        processFile = true;

                    if( processFile == true ) {
                        unsigned userIndex = internalDataLayer->GetUserIndex( );
                        for( unsigned timeIndex = startIndex; timeIndex < endIndex; timeIndex++ ) {
                            for( unsigned latitudeIndex = 0; latitudeIndex < Parameters::Get( )->GetLengthDataLatitudeArray( ); latitudeIndex++ ) {
                                for( unsigned longitudeIndex = 0; longitudeIndex < Parameters::Get( )->GetLengthDataLongitudeArray( ); longitudeIndex++ ) {

                                    unsigned dataIndex = Processor::Get( )->Indices3DToIndex( longitudeIndex, latitudeIndex, timeIndex, Parameters::Get( )->GetLengthDataLongitudeArray( ), Parameters::Get( )->GetLengthDataLatitudeArray( ) );

                                    float dataLatitude = Parameters::Get( )->GetDataLatitudeAtIndex( latitudeIndex );
                                    float dataLongitude = Parameters::Get( )->GetDataLongitudeAtIndex( longitudeIndex );

                                    if( dataLongitude >= Parameters::Get( )->GetUserLongitudeAtIndex( 0 ) && dataLongitude <= Parameters::Get( )->GetUserLongitudeAtIndex( Parameters::Get( )->GetLengthUserLongitudeArray( ) - 1 ) && dataLatitude >= Parameters::Get( )->GetUserLatitudeAtIndex( 0 ) && dataLatitude <= Parameters::Get( )->GetUserLatitudeAtIndex( Parameters::Get( )->GetLengthUserLatitudeArray( ) - 1 ) ) {
                                        float data = readData[ dataIndex ];

                                        internalDataLayer->SetDataAtIndex( userIndex, data );
                                        userIndex += 1;
                                    }
                                }
                            }
                        }
                    } // else, skipping processing - file not relevant to the configuration
                }
            } catch( netCDF::exceptions::NcException& e ) {
                return false;
            }
            if( readData != NULL ) delete[ ] readData;
            if( timeData != NULL ) delete[ ] timeData;
        }
    }
    return DataLayerSet::Get( )->VerifyDataLayers( );
}

unsigned FileReader::ClassifyNetCDFFile( const std::string& filePath ) {
    unsigned retVal = Constants::eUnclassified;
    if( Strings::Get( )->IsSubStringInString( "historical", filePath ) == true )
        retVal = Constants::eHistorical;
    else if( Strings::Get( )->IsSubStringInString( "rcp", filePath ) == true )
        retVal = Constants::eFuture;

    return retVal;
}

bool FileReader::ReadCatchData( ) {
    if( Parameters::Get( )->GetApplyCatchData( ) == true ) {
        CatchData::Get( )->Initialise( );
        if( ReadTextFile( Constants::cConfigurationDirectory + Constants::cFunctionalGroupLookupFileName ) == true ) {
            CatchData::Get( )->InitialiseFunctionalGroupLookup( mMetadata );
            if( ReadTextFile( Constants::cConfigurationDirectory + Constants::cCatchMetadataFileName ) == true )
                if( ReadCatchDataFile( mMetadata[ Constants::eCellIDTable ][ Constants::eCatchFilePath ], Constants::eCellIDTable ) == true )
                    return ReadCatchDataFile( mMetadata[ Constants::eCatchDataTable ][ Constants::eCatchFilePath ], Constants::eCatchDataTable );
        }
    } else
        return true;
}

bool FileReader::DefineFunctionalGroups( ) {
    if( FunctionalGroups::Get( )->SetStocks( Constants::cConfigurationDirectory + Constants::cStockDefinitionsFileName ) )
        return FunctionalGroups::Get( )->SetCohorts( Constants::cConfigurationDirectory + Constants::cCohortDefinitionsFileName );
    else
        return false;
}

bool FileReader::ReadStateFile( std::string& stateFile ) {
    if( Parameters::Get( )->GetReadModelState( ) == true ) {
        if( stateFile == "" ) stateFile = Constants::cStateFileName;
        return ReadTextFile( stateFile, false );
    } else
        return true;
}

void FileReader::ClearMetadata( ) {
    for( unsigned rowIndex = 0; rowIndex < mMetadata.size( ); rowIndex++ ) {
        mMetadata[ rowIndex ].clear( );
    }
    mMetadata.clear( );
    mMetadataHeadings.clear( );
}
