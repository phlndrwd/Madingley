#include "Parameters.h"

#include "Constants.h"
#include "Strings.h"
#include "Date.h"
#include "Processor.h"
#include "DataCoords.h"
#include "DataIndices.h"
#include "Timing.h"

Types::ParametersPointer Parameters::mThis = NULL;

Types::ParametersPointer Parameters::Get( ) {
    if( mThis == NULL ) {
        mThis = new Parameters( );
    }
    return mThis;
}

Parameters::~Parameters( ) {
    delete[ ] mTimeStepArray;
    delete[ ] mYearArray;
    delete[ ] mDataLongitudeArray;
    delete[ ] mDataLatitudeArray;
    delete[ ] mUserLongitudeArray;
    delete[ ] mUserLatitudeArray;

    if( mThis != NULL ) delete mThis;
}

Parameters::Parameters( ) {
    mExperimentName = " "; // Default to whitespace, as this will be ignored
    mRootDataDirectory = "";
    mStartDateString = "";
    mEndDateString = "";
    mSpinUpYears = 0;
    mApplyCatchData = false;
    mCatchCoefficient = 0;
    mUserMinimumLongitude = 0;
    mUserMaximumLongitude = 0;
    mUserMinimumLatitude = 0;
    mUserMaximumLatitude = 0;
    mGridCellSize = 0;
    mExtinctionThreshold = 0;
    mMaximumNumberOfCohortsPerCell = 0;
    mPlanktonSizeThreshold = 0;
    mReadModelState = false;
    mWriteModelState = false;
    mRandomSeed = 0;
}

bool Parameters::Initialise( const Types::StringMatrix& rawInputParameterData ) {
    bool success = false;
    if( rawInputParameterData.size( ) > 0 ) {
        if( rawInputParameterData[ 0 ].size( ) == Constants::eParameterValue + 1 ) {
            for( unsigned rowIndex = 0; rowIndex < rawInputParameterData.size( ); rowIndex++ ) {
                std::string parameterName = Strings::Get( )->ToLowercase( rawInputParameterData[ rowIndex ][ Constants::eParameterName ] );
                std::string parameterValue = rawInputParameterData[ rowIndex ][ Constants::eParameterValue ];

                if( parameterName == "experimentname" ) SetExperimentName( parameterValue );
                else if( parameterName == "rootdatadirectory" ) SetRootDataDirectory( parameterValue );
                else if( parameterName == "startdate" ) SetStartDateString( Strings::Get( )->ToLowercase( parameterValue ) );
                else if( parameterName == "enddate" ) SetEndDateString( Strings::Get( )->ToLowercase( parameterValue ) );
                else if( parameterName == "spinupyears" ) SetSpinUpYears( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "applycatchdata" ) SetApplyCatchData( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "catchcoefficient" ) SetCatchCoefficient( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "minimumlongitude" ) SetUserMinimumLongitude( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "maximumlongitude" ) SetUserMaximumLongitude( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "minimumlatitude" ) SetUserMinimumLatitude( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "maximumlatitude" ) SetUserMaximumLatitude( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "gridcellsize" ) SetGridCellSize( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "extinctionthreshold" ) SetExtinctionThreshold( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "maximumnumberofcohortspercell" ) SetMaximumNumberOfCohortsPerCell( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "planktonsizethreshold" ) SetPlanktonSizeThreshold( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "readmodelstate" ) SetReadModelState( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "writemodelstate" ) SetWriteModelState( Strings::Get( )->StringToNumber( parameterValue ) );
                else if( parameterName == "randomseed" ) SetRandomSeed( Strings::Get( )->StringToNumber( parameterValue ) );
            }
            success = CalculateParameters( );
        }
    }
    return success;
}

bool Parameters::CalculateParameters( ) {
    int lengthOfSimulationInMonths = Date::MonthsBetweenDates( mStartDateString, mEndDateString );
    if( lengthOfSimulationInMonths > 0 ) {
        // Build data base path
        mDataBasePath = Parameters::Get( )->GetRootDataDirectory( );
        if( mDataBasePath[ mDataBasePath.size( ) - 1 ] != Constants::cFolderDelimiter )
            mDataBasePath.append( Strings::Get( )->ToString( Constants::cFolderDelimiter ) );
        mDataBasePath.append( Strings::Get( )->ToString( Parameters::Get( )->GetGridCellSize( ) ) );
        mDataBasePath.append( "deg/" );

        // Calculate temporal parameters
        unsigned firstYearOfData = Timing::Get( )->GetFirstYear( );
        unsigned finalYearOfData = Timing::Get( )->GetLastYear( );
        unsigned dataEndYear = Date::GetYearOfDate( Constants::cDataHistoricalEndDate );

        if( firstYearOfData <= dataEndYear && finalYearOfData <= dataEndYear )
            mTemporalConfiguration = Constants::eHistoricalOnly;
        else if( firstYearOfData > dataEndYear )
            mTemporalConfiguration = Constants::eFutureOnly;
        else if( firstYearOfData <= dataEndYear && finalYearOfData > dataEndYear )
            mTemporalConfiguration = Constants::eHistoricalAndFuture;

        mLengthOfSimulationInMonths = lengthOfSimulationInMonths;
        mLengthOfSimulationInYears = mLengthOfSimulationInMonths / 12;

        mTimeStepArray = new unsigned[ mLengthOfSimulationInMonths ];
        for( unsigned monthIndex = 0; monthIndex < mLengthOfSimulationInMonths; monthIndex++ ) {
            mTimeStepArray[ monthIndex ] = monthIndex;
        }

        mYearArray = new unsigned[ mLengthOfSimulationInYears ];
        unsigned firstYearOfConfiguration = Date::GetYearOfDate( mStartDateString );
        for( unsigned yearIndex = 0; yearIndex < mLengthOfSimulationInYears; yearIndex++ ) {
            mYearArray[ yearIndex ] = firstYearOfConfiguration + yearIndex;
        }

        if( mGridCellSize == 0.5 || mGridCellSize == 1 || mGridCellSize == 2 || mGridCellSize == 5 || mGridCellSize == 10 ) {
            // Calculate spatial parameters
            mLengthDataLongitudeArray = 360 / mGridCellSize;
            mDataLongitudeArray = new float[ mLengthDataLongitudeArray ];
            for( unsigned longitudeIndex = 0; longitudeIndex < mLengthDataLongitudeArray; longitudeIndex++ ) {
                mDataLongitudeArray[ longitudeIndex ] = ( -180 + ( ( float ) mGridCellSize / 2 ) ) + ( longitudeIndex * ( float ) mGridCellSize );
            }

            mLengthDataLatitudeArray = 180 / mGridCellSize;
            mDataLatitudeArray = new float[ mLengthDataLatitudeArray ];
            for( unsigned latitudeIndex = 0; latitudeIndex < mLengthDataLatitudeArray; latitudeIndex++ ) {
                mDataLatitudeArray[ latitudeIndex ] = ( -90 + ( ( float ) mGridCellSize / 2 ) ) + ( latitudeIndex * ( float ) mGridCellSize );
            }
        } else {
            std::cout << "ERROR> Grid cell size is not configured to an allowable value (0.5, 1, 2, 5 or 10). System exiting..." << std::endl;
            exit( 1 );
        }

        if( mUserMinimumLongitude <= mUserMaximumLongitude ) {
            mDataIndexOfUserMinimumLongitude = Processor::Get( )->CalculateArrayIndexOfValue( mDataLongitudeArray, mLengthDataLongitudeArray, mUserMinimumLongitude );
            mDataIndexOfUserMaximumLongitude = Processor::Get( )->CalculateArrayIndexOfValue( mDataLongitudeArray, mLengthDataLongitudeArray, mUserMaximumLongitude );
            mLengthUserLongitudeArray = ( mDataIndexOfUserMaximumLongitude - mDataIndexOfUserMinimumLongitude ) + 1;

            mUserLongitudeArray = new float[ mLengthUserLongitudeArray ];
            for( unsigned userLongitudeIndex = 0; userLongitudeIndex < mLengthUserLongitudeArray; userLongitudeIndex++ ) {
                mUserLongitudeArray[ userLongitudeIndex ] = mDataLongitudeArray[ userLongitudeIndex + mDataIndexOfUserMinimumLongitude ];
            }
        } else {
            std::cout << "ERROR> Configured minimum longitude is larger or the same as maximum. System exiting..." << std::endl;
            exit( 1 );
        }

        if( mUserMinimumLatitude <= mUserMaximumLatitude ) {
            mDataIndexOfUserMinimumLatitude = Processor::Get( )->CalculateArrayIndexOfValue( mDataLatitudeArray, mLengthDataLatitudeArray, mUserMinimumLatitude );
            mDataIndexOfUserMaximumLatitude = Processor::Get( )->CalculateArrayIndexOfValue( mDataLatitudeArray, mLengthDataLatitudeArray, mUserMaximumLatitude );
            mLengthUserLatitudeArray = ( mDataIndexOfUserMaximumLatitude - mDataIndexOfUserMinimumLatitude ) + 1;

            mUserLatitudeArray = new float[ mLengthUserLatitudeArray ];
            for( unsigned userLatitudeIndex = 0; userLatitudeIndex < mLengthUserLatitudeArray; userLatitudeIndex++ ) {
                mUserLatitudeArray[ userLatitudeIndex ] = mDataLatitudeArray[ userLatitudeIndex + mDataIndexOfUserMinimumLatitude ];
            }
        } else {
            std::cout << "ERROR> Configured minimum latitude is larger or the same as maximum. System exiting..." << std::endl;
            exit( 1 );
        }

        mNumberOfGridCells = mLengthUserLongitudeArray * mLengthUserLatitudeArray;
        mSizeOfMonthlyGridDatum = mNumberOfGridCells * mLengthOfSimulationInMonths;
        mSizeOfAnnualGridDatum = mNumberOfGridCells * mLengthOfSimulationInYears;

        unsigned cellIndex = 0;
        mCoordsIndicesLookup.resize( mNumberOfGridCells );
        for( unsigned latitudeIndex = 0; latitudeIndex < mLengthUserLatitudeArray; latitudeIndex++ ) {
            for( unsigned longitudeIndex = 0; longitudeIndex < mLengthUserLongitudeArray; longitudeIndex++ ) {

                float longitude = mUserLongitudeArray[ longitudeIndex ];
                float latitude = mUserLatitudeArray[ latitudeIndex ];

                Types::DataCoordsPointer coords = new DataCoords( longitude, latitude );
                Types::DataIndicesPointer indices = new DataIndices( longitudeIndex, latitudeIndex );

                mCoordsIndicesLookup[ cellIndex ] = std::make_pair( coords, indices );

                cellIndex += 1;
            }
        }
        return true;
    } else
        return false;
}

std::string Parameters::GetExperimentName( ) const {
    return mExperimentName;
}

std::string Parameters::GetRootDataDirectory( ) const {
    return mRootDataDirectory;
}

std::string Parameters::GetStartDateString( ) const {
    return mStartDateString;
}

std::string Parameters::GetEndDataString( ) const {
    return mEndDateString;
}

unsigned Parameters::GetSpinUpYears( ) const {
    return mSpinUpYears;
}

bool Parameters::GetApplyCatchData( ) const {
    return mApplyCatchData;
}

float Parameters::GetCatchCoefficient( ) const {
    return mCatchCoefficient;
}

int Parameters::GetUserMinimumLongitude( ) const {
    return mUserMinimumLongitude;
}

int Parameters::GetUserMaximumLongitude( ) const {
    return mUserMaximumLongitude;
}

int Parameters::GetUserMinimumLatitude( ) const {
    return mUserMinimumLatitude;
}

int Parameters::GetUserMaximumLatitude( ) const {
    return mUserMaximumLatitude;
}

float Parameters::GetGridCellSize( ) const {
    return mGridCellSize;
}

float Parameters::GetExtinctionThreshold( ) const {
    return mExtinctionThreshold;
}

unsigned Parameters::GetMaximumNumberOfCohortsPerCell( ) const {
    return mMaximumNumberOfCohortsPerCell;
}

float Parameters::GetPlanktonSizeThreshold( ) const {
    return mPlanktonSizeThreshold;
}

bool Parameters::GetReadModelState( ) const {
    return mReadModelState;
}

bool Parameters::GetWriteModelState( ) const {
    return mWriteModelState;
}

unsigned Parameters::GetRandomSeed( ) const {
    return mRandomSeed;
}

void Parameters::SetExperimentName( const std::string& experimentName ) {
    mExperimentName = experimentName;
}

void Parameters::SetRootDataDirectory( const std::string& rootDataDirectory ) {
    mRootDataDirectory = rootDataDirectory;
}

void Parameters::SetStartDateString( const std::string& startDateString ) {
    mStartDateString = startDateString;
}

void Parameters::SetEndDateString( const std::string& endDateString ) {
    mEndDateString = endDateString;
}

void Parameters::SetSpinUpYears( const unsigned& spinUpYears ) {
    mSpinUpYears = spinUpYears;
}

void Parameters::SetApplyCatchData( const bool applyCatchData ) {
    mApplyCatchData = applyCatchData;
}

void Parameters::SetCatchCoefficient( const float& catchCoefficient ) {
    mCatchCoefficient = catchCoefficient;
}

void Parameters::SetUserMinimumLongitude( const int& userMinimumLongitude ) {
    mUserMinimumLongitude = userMinimumLongitude;
}

void Parameters::SetUserMaximumLongitude( const int& userMaximumLongitude ) {
    mUserMaximumLongitude = userMaximumLongitude;
}

void Parameters::SetUserMinimumLatitude( const int& userMinimumLatitude ) {
    mUserMinimumLatitude = userMinimumLatitude;
}

void Parameters::SetUserMaximumLatitude( const int& userMaximumLatitude ) {
    mUserMaximumLatitude = userMaximumLatitude;
}

void Parameters::SetGridCellSize( const float& gridCellSize ) {
    mGridCellSize = gridCellSize;
}

void Parameters::SetExtinctionThreshold( const float& extinctionThreshold ) {
    mExtinctionThreshold = extinctionThreshold;
}

void Parameters::SetMaximumNumberOfCohortsPerCell( const unsigned& maximumNumberOfCohorts ) {
    mMaximumNumberOfCohortsPerCell = maximumNumberOfCohorts;
}

void Parameters::SetPlanktonSizeThreshold( const float& planktonSizeThreshold ) {
    mPlanktonSizeThreshold = planktonSizeThreshold;
}

void Parameters::SetReadModelState( const bool readModelState ) {
    mReadModelState = readModelState;
}

void Parameters::SetWriteModelState( const bool writeModelState ) {
    mWriteModelState = writeModelState;
}

void Parameters::SetRandomSeed( const unsigned& randomSeed ) {
    mRandomSeed = randomSeed;
}

unsigned Parameters::GetTemporalConfiguration( ) const {
    return mTemporalConfiguration;
}

std::string& Parameters::GetDataBasePath( ) {
    return mDataBasePath;
}

unsigned Parameters::GetLengthOfSimulationInMonths( ) const {
    return mLengthOfSimulationInMonths;
}

unsigned Parameters::GetLengthOfSimulationInYears( ) const {
    return mLengthOfSimulationInYears;
}

unsigned Parameters::GetNumberOfGridCells( ) const {
    return mNumberOfGridCells;
}

unsigned Parameters::GetLengthDataLongitudeArray( ) const {
    return mLengthDataLongitudeArray;
}

unsigned Parameters::GetLengthDataLatitudeArray( ) const {
    return mLengthDataLatitudeArray;
}

unsigned Parameters::GetDataIndexOfUserMinimumLongitude( ) const {
    return mDataIndexOfUserMinimumLongitude;
}

unsigned Parameters::GetDataIndexOfUserMaximumLongitude( ) const {
    return mDataIndexOfUserMaximumLongitude;
}

unsigned Parameters::GetDataIndexOfUserMinimumLatitude( ) const {
    return mDataIndexOfUserMinimumLatitude;
}

unsigned Parameters::GetDataIndexOfUserMaximumLatitude( ) const {
    return mDataIndexOfUserMaximumLatitude;
}

unsigned Parameters::GetLengthUserLongitudeArray( ) const {
    return mLengthUserLongitudeArray;
}

unsigned Parameters::GetLengthUserLatitudeArray( ) const {
    return mLengthUserLatitudeArray;
}

unsigned Parameters::GetSizeOfMonthlyGridDatum( ) const {
    return mSizeOfMonthlyGridDatum;
}

unsigned Parameters::GetSizeOfAnnualGridDatum( ) const {
    return mSizeOfAnnualGridDatum;
}

float Parameters::GetDataLongitudeAtIndex( const unsigned& index ) const {
    return mDataLongitudeArray[ index ];
}

float Parameters::GetDataLatitudeAtIndex( const unsigned& index ) const {
    return mDataLatitudeArray[ index ];
}

float Parameters::GetUserLongitudeAtIndex( const unsigned& index ) const {
    return mUserLongitudeArray[ index ];
}

float Parameters::GetUserLatitudeAtIndex( const unsigned& index ) const {
    return mUserLatitudeArray[ index ];
}

unsigned* Parameters::GetTimeStepArray( ) const {
    return mTimeStepArray;
}

unsigned* Parameters::GetYearArray( ) const {
    return mYearArray;
}

float* Parameters::GetUserLongitudeArray( ) const {
    return mUserLongitudeArray;
}

float* Parameters::GetUserLatitudeArray( ) const {
    return mUserLatitudeArray;
}

int Parameters::GetCellIndexFromDataIndices( const unsigned& longitudeIndex, const unsigned& latitudeIndex ) const {

    int cellIndex = Constants::cMissingValue;
    for( unsigned index = 0; index < mNumberOfGridCells; index++ ) {
        Types::DataIndicesPointer indices = mCoordsIndicesLookup[ index ].second;

        if( indices->GetX( ) == longitudeIndex && indices->GetY( ) == latitudeIndex ) {
            cellIndex = index;
            break;
        }
    }
    return cellIndex;
}

Types::DataCoordsPointer Parameters::GetDataCoordsFromCellIndex( const unsigned& cellIndex ) const {
    return mCoordsIndicesLookup[ cellIndex ].first;
}

Types::DataIndicesPointer Parameters::GetDataIndicesFromCellIndex( const unsigned& cellIndex ) const {
    return mCoordsIndicesLookup[ cellIndex ].second;
}
