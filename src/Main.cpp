#include "Madingley.h"

#include "FileReader.h"
#include "FileWriter.h"
#include "Date.h"
#include "Strings.h"

#include <fenv.h>

int main( int numberOfArguments, char* commandlineArguments[ ] ) {
    std::string stateFile = "";
    if( numberOfArguments > 1 ) stateFile = commandlineArguments[ 1 ];

    //this line enables the gdb debugger to catch Nan or floating point problems
    feraiseexcept( FE_INVALID | FE_OVERFLOW );
    // Write out model details to the console
    std::cout << "Madingley v0.1" << std::endl;
    std::cout << "Run started at " << Date::GetDateAndTimeString( Constants::cCompleteDateFormat ) << std::endl;

    FileReader fileReader;
    fileReader.ReadFiles( );

    FileWriter fileWriter;
    fileReader.ReadData( stateFile ); // Time-consuming data file reading
    fileWriter.WriteInputFiles( ); // Write additional meta-data files

    Madingley madingley;

    // Declare and start a timer
    Stopwatch timer;
    timer.Start( );

    // Run the simulation
    madingley.Run( );

    fileWriter.WriteFiles( madingley );

    // Stop the timer and write out the time taken to run this simulation
    timer.Stop( );
    std::cout << "Model run finished" << std::endl;
    std::cout << "Total elapsed time was " + Strings::Get( )->ToString( timer.GetElapsedTimeSecs( ) ) + " seconds." << std::endl;

    return 0;
}
