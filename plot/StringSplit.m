function[ stringCells ] = StringSplit( string, delimiterCharacter )

if exist( 'delimiterCharacter', 'var' ) == 0
    delimiterCharacter = ' ';
end

lengthString = length( string );

charCount = 0;
for i = 1:lengthString
    if strcmp( string( i ), delimiterCharacter ) == 1
        charCount = charCount + 1;
    end
end

stringCells = cell( 1, charCount + 1 );
firstPos = 1;
cellCount = 1;
for i = 2:lengthString
    if strcmp( string( i ), delimiterCharacter ) == 1
        stringCells{ cellCount } = string( firstPos:i - 1 );
        firstPos = i + 1;
        cellCount = cellCount + 1;
    elseif  i == lengthString
        stringCells{ cellCount } = string( firstPos:i );
    end
end
