function[ data ] = ReadTable( filePath, delimiterCharacter )

if exist( 'delimiterCharacter', 'var' ) == 0
    delimiterCharacter = ',';
end

fid = fopen( filePath );
rowCount = 0;
if fid ~= -1
    while ~feof( fid )
        line = fgetl( fid );
        if rowCount == 0
            lineCells = StringSplit( line, delimiterCharacter );
            columnCount = length( lineCells );
        end
        if strcmp( line( 1 ), '#' ) == 0
            rowCount = rowCount + 1;
        end
    end
    fclose( fid );
    
    data = cell( rowCount - 1, columnCount );
    
    fid = fopen( filePath );
    rowCount = 0;
    while ~feof( fid )
        line = fgetl( fid );
        if rowCount > 0
            lineCells = StringSplit( line, delimiterCharacter );
            data( rowCount, : ) = lineCells;
        end
        if strcmp( line( 1 ), '#' ) == 0
            rowCount = rowCount + 1;
        end
    end
    fclose( fid );
else
    data = [];
end
