
%% Load Meta Data
OutputParameters = ReadTable( [ optionOutputDirectory optionCurrentDataSet optionOutputParametersFile ], ',' );

numberOfDatums = length( OutputParameters( :, 1 ) );

annualGridIndices = [ ];
monthlyGridIndices = [ ];
noTimeGridIndices = [ ];

counters = zeros( 1, 3 );

for datumIndex = 1:numberOfDatums
    if strcmpi( OutputParameters{ datumIndex, 2 }, 'grid' ) == 1 && strcmpi( OutputParameters{ datumIndex, 3 }, 'year' ) == 1
        counters( 1 ) = counters( 1 ) + 1;
        annualGridIndices( counters( 1 ) ) = datumIndex;
    elseif strcmpi( OutputParameters{ datumIndex, 2 }, 'grid' ) == 1 && strcmpi( OutputParameters{ datumIndex, 3 }, 'month' ) == 1
        counters( 2 ) = counters( 2 ) + 1;
        monthlyGridIndices( counters( 2 ) ) = datumIndex;
    elseif strcmpi( OutputParameters{ datumIndex, 2 }, 'grid' ) == 1 && strcmpi( OutputParameters{ datumIndex, 3 }, 'none' ) == 1
        counters( 3 ) = counters( 3 ) + 1;
        noTimeGridIndices( counters( 3 ) ) = datumIndex;
    end
end
%% Annual grid plots
annualGridFile = [ optionOutputDirectory optionCurrentDataSet optionAnnualGridFile ];
if exist( annualGridFile, 'file' ) == 2
    for plotIndex = 1:length( annualGridIndices )
        dataSetName = OutputParameters{ annualGridIndices( plotIndex ), 1 };
        try
            dataSet = ncread( annualGridFile, dataSetName );
        catch
            disp( [ 'ERROR> Unable to plot variable "' OutputParameters{ annualGridIndices( plotIndex ) } '" in ' optionAnnualGridFile '.' ] );
        end
    end
else
    disp( [ 'Output file ' annualGridFile ' does not exist.' ] );
end
%% Monthly grid plots
monthlyGridFile = [ optionOutputDirectory optionCurrentDataSet optionMonthlyGridFile ];
if exist( monthlyGridFile, 'file' ) == 2
    longitude = ncread( monthlyGridFile, 'lon' );
    latitude = ncread( monthlyGridFile, 'lat' );
    
    for plotIndex = 1:length( monthlyGridIndices )
        dataSetName = OutputParameters{ monthlyGridIndices( plotIndex ), 1 };
        colourbarLabel = OutputParameters{ monthlyGridIndices( plotIndex ), 4 };
        try
            dataSet = ncread( monthlyGridFile, dataSetName );
            dataSet( dataSet == 0 ) = NaN;
            temporalResolution = size( dataSet, 3 );
            if optionMonthIndex <= temporalResolution
                handle = figure;
                pcolor( longitude, latitude, dataSet( :, :, optionMonthIndex )' ), shading flat;
                c = colorbar;
                ylabel( c, colourbarLabel );
                xlabel( 'Longitude' );
                ylabel( 'Latitude' );
                title( dataSetName );
                
                if optionPrintPlotsToFile == 1
                    disp( [ optionOutputDirectory optionCurrentDataSet dataSetName '.' optionOutputFileFormat  ] );
                    printPlotToFile( handle, [ optionPlotImageWidth optionPlotImageHeight ], [ optionOutputDirectory optionCurrentDataSet dataSetName ], optionOutputFileFormat, 1 );
                    close( handle );
                end
            else
                disp( [ 'ERROR> optionMonthIndex is set to ' num2str( optionMonthIndex ) '. Data only has a temporal resolution of ' num2str( temporalResolution ) '.' ] );
            end
        catch
            disp( [ 'ERROR> Unable to plot variable "' OutputParameters{ monthlyGridIndices( plotIndex ) } '" in ' optionMonthlyGridFile '.' ] );
        end
    end
else
    disp( [ 'Output file ' monthlyGridFile ' does not exist.' ] );
end
%% Timless grid plots
noTimeGridFile = [ optionOutputDirectory optionCurrentDataSet optionNoTimeGridFile ];
if exist( noTimeGridFile, 'file' ) == 2
    longitude = ncread( noTimeGridFile, 'lon' );
    latitude = ncread( noTimeGridFile, 'lat' );
    
    for plotIndex = 1:length( noTimeGridIndices )
        dataSetName = OutputParameters{ noTimeGridIndices( plotIndex ), 1 };
        colourbarLabel = OutputParameters{ noTimeGridIndices( plotIndex ), 4 };
        try
            dataSet = ncread( noTimeGridFile, dataSetName );
            dataSet( dataSet == 0 ) = NaN;
            temporalResolution = size( dataSet, 3 );
            handle = figure;
            pcolor( longitude, latitude, dataSet( :, : )' ), shading flat;
            c = colorbar;
            ylabel( c, colourbarLabel );
            xlabel( 'Longitude' );
            ylabel( 'Latitude' );
            title( dataSetName );
            
            %disp( [ lower( dataSetName ) ', ' num2str( strfind( lower( dataSetName ), 'catch' ) ) ] );
            %if strfind( lower( dataSetName ), 'catch' ) == 1
            %    disp( [ 'CATCH GRID!!! ' dataSetName ] );
            %    caxis( [ 0 1.0000e+11 ] );
            %end
            
            if optionPrintPlotsToFile == 1
                disp( [ optionOutputDirectory optionCurrentDataSet dataSetName '.' optionOutputFileFormat  ] );
                printPlotToFile( handle, [ optionPlotImageWidth optionPlotImageHeight ], [ optionOutputDirectory optionCurrentDataSet dataSetName ], optionOutputFileFormat, 1 );
                close( handle );
            end
        catch
            disp( [ 'ERROR> Unable to plot variable "' OutputParameters{ noTimeGridIndices( plotIndex ) } '" in ' noTimeGridFile '.' ] );
        end
    end
else
    disp( [ 'Output file ' monthlyGridFile ' does not exist.' ] );
end