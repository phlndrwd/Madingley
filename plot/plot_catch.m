tic
clear

%% User Defined Parameters
optionExperimentDirectory       = '/home/philju/Dropbox/Development/NereusMadingley/output/fishing_pressure/';

optionOutputVariablesFile       = 'OutputVariables.csv';
optionCohortDefinitionsFile     = 'CohortDefinitions.csv';
optionAnnualGroupFile           = 'AnnualGroupOutputs.nc';

optionCatchTotalName            = 'CatchTotalGroup';
optionCatchAppliedName          = 'CatchAppliedGroup';
optionCatchMissedName           = 'CatchMissedGroup';
optionTotalGroupMassName        = 'TotalGroupCohortMass';

cutOffThresholds                = [ .4998 .6 .7 .8 .9 ];
optionMissingValue              = -9999;
optionOutputFileFormat          = 'png'; % EPS or PNG
optionPlotImageWidth            = 19; % cm
optionPlotImageHeight           = 16; % cm

catchTotalIndex = optionMissingValue;
catchAppliedIndex = optionMissingValue;
catchMissedIndex = optionMissingValue;

%% Input Formatting
if strcmp( optionExperimentDirectory( end ), '/' ) == 0
    optionExperimentDirectory = [ optionExperimentDirectory '/' ];
end

numberOfCutoffThresholds = length( cutOffThresholds );

dataSetListing = dir( optionExperimentDirectory );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
availableBiomasses = zeros( 10, numberOfCutoffThresholds ); % Hard-coded constant
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
totalCohortBiomass = zeros( 1, length( dataSetListing )-2 );

dataSetIndex = 0;
for dataListingIndex = 1:length( dataSetListing )
    dataSetName = dataSetListing( dataListingIndex ).name;
    if dataSetName( 1 ) ~= '.' && dataSetListing( dataListingIndex ).isdir ~= 0
        dataSetIndex = dataSetIndex + 1;
        %% Load Meta Data
        inputDirectory = [ optionExperimentDirectory dataSetName '/' ];
        
        if dataSetIndex == 1
            OutputVariables = ReadTable( [ inputDirectory optionOutputVariablesFile ], ',' );
            CohortDefinitions = ReadTable( [ inputDirectory optionCohortDefinitionsFile ], ',' );
            
            numberOfDatums = length( OutputVariables( :, 1 ) );
            
            for datumIndex = 1:numberOfDatums
                if strcmpi( OutputVariables{ datumIndex, 1 }, optionCatchTotalName ) == 1
                    catchTotalIndex = datumIndex;
                elseif strcmpi( OutputVariables{ datumIndex, 1 }, optionCatchAppliedName ) == 1
                    catchAppliedIndex = datumIndex;
                elseif strcmpi( OutputVariables{ datumIndex, 1 }, catchMissedIndex ) == 1
                    catchMissedIndex = datumIndex;
                end
            end
        end
        %%
        annualGroupFile = [ inputDirectory optionAnnualGroupFile ];
        
        if exist( annualGroupFile, 'file' ) == 2
            %try
            if dataSetIndex == 1
                catchTotalPerGroup = ncread( annualGroupFile, optionCatchTotalName );
                catchTotalPerGroup( catchTotalPerGroup == optionMissingValue ) = NaN;
                
                functionalGroups = ncread( annualGroupFile, 'group' );
                numberOfFunctionalGroups = length( functionalGroups )-4;
                
                sumCatchTotalPerGroup = sum( catchTotalPerGroup );
                totalCatch = sum( sumCatchTotalPerGroup );
                
                fractionCatchTotalPerGroup = sumCatchTotalPerGroup ./ totalCatch;
                %months = {'Obligate zooplankton','Small pelagics','Medium pelagics','Large pelagics','Small demersals','Medium demersals','Large demersals','Small bathypelagics','Medium bathypelagics','Large bathypelagics','Small bathydemersals','Medium bathydemersals','Large bathydemersals','Small benthopelagics','Medium benthopelagics','Large benthopelagics','Small reef associated fish','Medium reef associated fish','Large reef associated fish','Small to medium sharks','Large sharks','Small to medium rays','Large rays','Small to medium flatfishes','Large flatfishes','Cephalopods','Small crustaceans','Medium crustaceans','Jellyfish','Other invertebrates','Krill','Small endotherms','Large endotherms','Baleen whales'};
                
                [ sortedFractionCatchTotalPerGroup, sortedGroupIndices ] = sort( fractionCatchTotalPerGroup, 'descend' );
                
                cohortLegendText = CohortDefinitions( :, 14 );
                cohortLegendText = cohortLegendText( sortedGroupIndices );
                cohortLegendText = cohortLegendText( 1:numberOfFunctionalGroups );
                
                %% Calculate the final index in the sorted list that
                % would meet the fractional contribution to total catch
                % defined by the cutoff threshold.
                thresholdFinalIndex = zeros( 1, numberOfCutoffThresholds );
                for thresholdIndex = 1:numberOfCutoffThresholds
                    fracSum = 0;
                    for groupIndex = 1:numberOfFunctionalGroups
                        fracSum = fracSum + sortedFractionCatchTotalPerGroup( groupIndex );
                        if fracSum >= cutOffThresholds( thresholdIndex )
                            thresholdFinalIndex( thresholdIndex ) = groupIndex;
                            break;
                        end
                    end
                    
                    cutOffThresholds( 1 ) = 0.5;
                    handlePlot = figure;
                    barsToPlot = sortedFractionCatchTotalPerGroup( 1:numberOfFunctionalGroups );
                    handle = bar( barsToPlot, 'FaceColor', [ 0.78 0 0 ] );
                    hold on;
                    barsToPlot( 1:thresholdFinalIndex( thresholdIndex ) ) = NaN;
                    bar( barsToPlot );
                    title( [ num2str( cutOffThresholds( thresholdIndex ) ) ' of Total Catch' ] );

                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % Reduce the size of the axis so that all the labels fit in the figure.
                    pos = get( gca, 'Position' );
                    set( gca, 'Position', [ pos( 1 ), .25, pos( 3 ) .65 ] );
                    % Place the text labels
                    t = text( 1:numberOfFunctionalGroups, zeros( 1, numberOfFunctionalGroups ), cohortLegendText );
                    set( t, 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top', 'Rotation', 45 );
                    % Remove the default labels
                    set( gca, 'XTickLabel', '' );
                    % Get the Extent of each text object.  This
                    % loop is unavoidable.
                    for i = 1:length( t )
                        ext( i, : ) = get( t( i ), 'Extent' );
                    end
                    % Determine the lowest point.  The X-label will be
                    % placed so that the top is aligned with this point.
                    LowYPoint = min( ext( :, 2 ) );
                    % Place the axis label at this point
                    XMidPoint = 1 + ( numberOfFunctionalGroups-1 )/2;
                    tl = text( XMidPoint, LowYPoint, 'X-Axis Label', 'VerticalAlignment','top', 'HorizontalAlignment','center' );
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    xlim( [ 1 30 ] );
                    ylabel( 'Normalised Contribution to Catch' );
                    printPlotToFile( handlePlot, [ optionPlotImageWidth optionPlotImageHeight ], [ optionExperimentDirectory num2str( cutOffThresholds( thresholdIndex ) ) '_Threshold' ], optionOutputFileFormat );
                    close( handlePlot );
                end
                
            end
            disp( annualGroupFile );
            totalGroupCohortMass = ncread( annualGroupFile, optionTotalGroupMassName );
            sumGroupCohortMass = sum( totalGroupCohortMass );
            totalCohortBiomass( dataSetIndex ) = sum( sumGroupCohortMass );
            sortedSumGroupCohortMass = sumGroupCohortMass( sortedGroupIndices );
            
            for thresholdIndex = 1:numberOfCutoffThresholds
                availableBiomasses( dataSetIndex, thresholdIndex ) = sum( sortedSumGroupCohortMass( 1:thresholdFinalIndex( thresholdIndex ) ) );
            end
            %catch
            %    disp( 'There was an error...' );
            %end
        else
            disp( [ 'Output file ' annualGroupFile ' does not exist.' ] );
        end
    end
end

optionPlotImageWidth            = 12; % cm
optionPlotImageHeight           = 10; % cm

xAxis = 0.1:0.1:1;
for plotIndex = 1:numberOfCutoffThresholds
    handlePlot = figure;
    plot( xAxis, availableBiomasses( :, plotIndex ) );
    xlim( [ 0.1 1 ] );
    xlabel( 'Coefficient on Fishing Pressure' );
    ylabel( 'Available Biomass' )
    title( [ 'On ' num2str( thresholdFinalIndex( plotIndex ) ) ' Most Exploited Groups ' ] );
    printPlotToFile( handlePlot, [ optionPlotImageWidth optionPlotImageHeight ], [ optionExperimentDirectory num2str( cutOffThresholds( plotIndex ) ) '_FishingPressureBiomassRel' ], optionOutputFileFormat );
    close( handlePlot );
end

toc