
%% Load Meta Data
OutputParameters = ReadTable( [ optionOutputDirectory optionCurrentDataSet optionOutputParametersFile ], ',' );
CohortDefinitions = ReadTable( [ optionOutputDirectory optionCurrentDataSet optionCohortDefinitionsFile ], ',' );

numberOfDatums = length( OutputParameters( :, 1 ) );

annualGroupIndices = [ ];
monthlyGroupIndices = [ ];

counters = zeros( 1, 4 );

for datumIndex = 1:numberOfDatums
    if strcmpi( OutputParameters{ datumIndex, 2 }, 'group' ) == 1 && strcmpi( OutputParameters{ datumIndex, 3 }, 'year' ) == 1
        counters( 2 ) = counters( 2 ) + 1;
        annualGroupIndices( counters( 2 ) ) = datumIndex;
    elseif strcmpi( OutputParameters{ datumIndex, 2 }, 'group' ) == 1 && strcmpi( OutputParameters{ datumIndex, 3 }, 'month' ) == 1
        counters( 4 ) = counters( 4 ) + 1;
        monthlyGroupIndices( counters( 4 ) ) = datumIndex;
    end
end
%% Annual group plots
annualGroupFile = [ optionOutputDirectory optionCurrentDataSet optionAnnualGroupFile ];
if exist( annualGroupFile, 'file' ) == 2
    for plotIndex = 1:length( annualGroupIndices )
        dataSetName = OutputParameters{ annualGroupIndices( plotIndex ), 1 };
        yAxisLabel = OutputParameters{ annualGroupIndices( plotIndex ), 4 };
        try
            if plotIndex == 1
                functionalGroups = ncread( annualGroupFile, 'group' );
                numberOfFunctionalGroups = length( functionalGroups );
            end
            dataSet = ncread( annualGroupFile, dataSetName );
            dataSet( dataSet == optionMissingValue ) = NaN;
            time = ncread( annualGroupFile, 'time' );
            
            handlePlot = figure;
            area( time, dataSet );
            colormap( jet( numberOfFunctionalGroups ) );
            xlabel( 'Year' );
            ylabel( yAxisLabel );
            title( dataSetName );
            xlim( [ min( time ) max( time ) ] );
            
            if ~isempty( strfind( lower( dataSetName ), 'catch' ) )
                ylim( [ 0 1.4e+11 ] );
            end
            
            handleLegend = legend( CohortDefinitions{ 1:numberOfFunctionalGroups, optionGroupLegendColumn }, 'location', 'EastOutside' );
            set( handleLegend, 'FontSize', optionLegendTextSize );
            
            if optionPrintPlotsToFile == 1
                disp( [ optionOutputDirectory optionCurrentDataSet dataSetName '.' optionOutputFileFormat  ] );
                printPlotToFile( handlePlot, [ optionPlotImageWidth + optionLegendWidthAddition optionPlotImageHeight ], [ optionOutputDirectory optionCurrentDataSet dataSetName ], optionOutputFileFormat );
                close( handlePlot );
            end
        catch
            disp( [ 'ERROR> Unable to plot variable "' OutputParameters{ annualGroupIndices( plotIndex ) } '" in ' optionAnnualGroupFile '.' ] );
        end
    end
else
    disp( [ 'Output file ' annualGroupFile ' does not exist.' ] );
end
%% Monthly group plots
monthlyGroupFile = [ optionOutputDirectory optionCurrentDataSet optionMonthlyGroupFile ];
if exist( monthlyGroupFile, 'file' ) == 2
    for plotIndex = 1:length( monthlyGroupIndices )
        dataSetName = OutputParameters{ monthlyGroupIndices( plotIndex ), 1 };
        yAxisLabel = OutputParameters{ monthlyGroupIndices( plotIndex ), 4 };
        try
            dataSet = ncread( monthlyGroupFile, dataSetName );
            dataSet( dataSet == optionMissingValue ) = NaN;
            time = ncread( monthlyGroupFile, 'time' );
            temporalResolution = size( dataSet, 3 );
            if plotIndex == 1
                functionalGroups = ncread( annualGroupFile, 'group' );
                numberOfFunctionalGroups = length( functionalGroups );
            end
            
            if ~isempty( strfind( lower( yAxisLabel ), 'log_{10}' ) )
                dataSet = log10( dataSet );
            end
            
            handlePlot = figure;
            %dataSet = dataSet';
            lineColours = jet( numberOfFunctionalGroups );
            for lineIndex = 1:numberOfFunctionalGroups
                plot( time, dataSet( :, lineIndex ), 'color', lineColours( lineIndex, : ) ), shading flat;
                hold on;
            end
            
            xlabel( 'Time Step' );
            ylabel( yAxisLabel );
            title( dataSetName );
            xlim( [ min( time ) max( time ) ] );
            handleLegend = legend( CohortDefinitions{ 1:numberOfFunctionalGroups, optionGroupLegendColumn }, 'location', 'EastOutside' );
            set( handleLegend, 'FontSize', optionLegendTextSize );
            
            if optionPrintPlotsToFile == 1
                disp( [ optionOutputDirectory optionCurrentDataSet dataSetName '.' optionOutputFileFormat  ] );
                printPlotToFile( handlePlot, [ optionPlotImageWidth + optionLegendWidthAddition optionPlotImageHeight ], [ optionOutputDirectory optionCurrentDataSet dataSetName ], optionOutputFileFormat );
                close( handlePlot );
            end
        catch
            disp( [ 'ERROR> Unable to plot variable "' OutputParameters{ monthlyGroupIndices( plotIndex ) } '" in ' optionMonthlyGroupFile '.' ] );
        end
    end
else
    disp( [ 'Output file ' monthlyGroupFile ' does not exist.' ] );
end