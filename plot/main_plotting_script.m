tic
clear

%% User Defined Parameters
optionCurrentDataSet            = 'Test/2018-06-01_15-29-54';
optionOutputDirectory           = '/home/philju/Dropbox/Madingley/MadingleyCPP_OMP/output/';

optionOutputParametersFile      = 'OutputVariables.csv';
optionCohortDefinitionsFile     = 'CohortDefinitions.csv';
optionAnnualBasicFile           = 'AnnualBasicOutputs.nc';
optionMonthlyBasicFile          = 'MonthlyBasicOutputs.nc';
optionAnnualGridFile            = 'AnnualGridOutputs.nc';
optionMonthlyGridFile           = 'MonthlyGridOutputs.nc';
optionNoTimeGridFile            = 'NoTimeGridOutputs.nc';
optionAnnualGroupFile           = 'AnnualGroupOutputs.nc';
optionMonthlyGroupFile          = 'MonthlyGroupOutputs.nc';

optionPrintPlotsToFile          = 1; % yes = 1, no = anything else
optionOutputFileFormat          = 'png'; % EPS or PNG
optionPlotImageWidth            = 12; % cm
optionPlotImageHeight           = 10; % cm
optionLegendWidthAddition       = 4; % cm
optionLegendTextSize            = 5;
optionGroupLegendColumn         = 14; % The column index of optionCohortDefinitionsFile that defines the legend for group plots

optionMissingValue              = -9999;
optionMonthIndex                = 12; % Time index for plotting grid datums

%% Input Formatting
if strcmp( optionOutputDirectory( end ), '/' ) == 0
    optionOutputDirectory = [ optionOutputDirectory '/' ];
end

if strcmp( optionCurrentDataSet( end ), '/' ) == 0
    optionCurrentDataSet = [ optionCurrentDataSet '/' ];
end

%% Plot Script Execution
if exist( [ optionOutputDirectory optionCurrentDataSet ], 'dir' ) == 7
    plot_basic_data
    plot_grid_data
    plot_group_data
else
    disp( 'ERROR> Input directories do not exist.' );
end
toc