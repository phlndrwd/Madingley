
%% Load Meta Data
OutputParameters = ReadTable( [ optionOutputDirectory optionCurrentDataSet optionOutputParametersFile ], ',' );

numberOfDatums = length( OutputParameters( :, 1 ) );

annualBasicIndices = [ ];
monthlyBasicIndices = [ ];

counters = zeros( 1, 4 );

for datumIndex = 1:numberOfDatums
    if strcmpi( OutputParameters{ datumIndex, 2 }, 'basic' ) == 1 && strcmpi( OutputParameters{ datumIndex, 3 }, 'year' ) == 1
        counters( 1 ) = counters( 1 ) + 1;
        annualBasicIndices( counters( 1 ) ) = datumIndex;
    elseif strcmpi( OutputParameters{ datumIndex, 2 }, 'basic' ) == 1 && strcmpi( OutputParameters{ datumIndex, 3 }, 'month' ) == 1
        counters( 3 ) = counters( 3 ) + 1;
        monthlyBasicIndices( counters( 3 ) ) = datumIndex;
    end
end
%% Annual basic plots
annualBasicFile = [ optionOutputDirectory optionCurrentDataSet optionAnnualBasicFile ];
if exist( annualBasicFile, 'file' ) == 2
    for plotIndex = 1:length( annualBasicIndices )
        dataSetName = OutputParameters{ annualBasicIndices( plotIndex ), 1 };
        yAxisLabel = OutputParameters{ annualBasicIndices( plotIndex ), 4 };
        try
            dataSet = ncread( annualBasicFile, dataSetName );
            time = ncread( annualBasicFile, 'time' );
            
            handle = figure;
            plot( time, dataSet );
            xlabel( 'Year' );
            ylabel( yAxisLabel );
            title( dataSetName );
            xlim( [ min( time ) max( time ) ] );
            
            if optionPrintPlotsToFile == 1
                disp( [ optionOutputDirectory optionCurrentDataSet dataSetName '.' optionOutputFileFormat  ] );
                printPlotToFile( handle, [ optionPlotImageWidth optionPlotImageHeight ], [ optionOutputDirectory optionCurrentDataSet dataSetName ], optionOutputFileFormat, 1 );
                close( handle );
            end
            
        catch
            disp( [ 'ERROR> Unable to plot variable "' OutputParameters{ annualBasicIndices( plotIndex ) } '" in ' optionAnnualBasicFile '.' ] );
        end
    end
else
    disp( [ 'Output file ' annualBasicFile ' does not exist.' ] );
end
%% Monthly basic plots
monthlyBasicFile = [ optionOutputDirectory optionCurrentDataSet optionMonthlyBasicFile ];
if exist( monthlyBasicFile, 'file' ) == 2
    for plotIndex = 1:length( monthlyBasicIndices )
        dataSetName = OutputParameters{ monthlyBasicIndices( plotIndex ), 1 };
        yAxisLabel = OutputParameters{ monthlyBasicIndices( plotIndex ), 4 };
        try
            dataSet = ncread( monthlyBasicFile, dataSetName );
            time = ncread( monthlyBasicFile, 'time' );
            
            handle = figure;
            plot( time, dataSet );
            xlabel( 'Time Step' );
            ylabel( yAxisLabel );
            title( dataSetName );
            xlim( [ min( time ) max( time ) ] );
            %if ~isempty( strfind( lower( dataSetName ), 'cohortabundance' ) )
            %    ylim( [ 0 7e+19 ] );
            %end
            
            if optionPrintPlotsToFile == 1
                disp( [ optionOutputDirectory optionCurrentDataSet dataSetName '.' optionOutputFileFormat  ] );
                printPlotToFile( handle, [ optionPlotImageWidth optionPlotImageHeight ], [ optionOutputDirectory optionCurrentDataSet dataSetName ], optionOutputFileFormat, 1 );
                close( handle );
            end
        catch
            disp( [ 'ERROR> Unable to plot variable "' OutputParameters{ monthlyBasicIndices( plotIndex ) } '" in ' optionMonthlyBasicFile '.' ] );
        end
    end
else
    disp( [ 'Output file ' monthlyBasicFile ' does not exist.' ] );
end