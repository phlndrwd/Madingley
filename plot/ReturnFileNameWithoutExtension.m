function[ fileNameWithoutExtension ] = ReturnFileNameWithoutExtension( fileName )

fileNameWithoutExtension = fileName( 1:length( fileName ) - ReturnFileExtensionLength( fileName ) );
