function[ fileNameExtension ] = ReturnFileNameExtension( fileName )

fileNameExtension = fileName( length( fileName ) - ReturnFileExtensionLength( fileName ) + 2:end );
